
USE [DEMO]
GO
/****** Object:  Table [dbo].[BussinessGroup]    Script Date: 4/26/2021 4:10:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BussinessGroup](
	[Id] [varchar](150) NOT NULL,
	[Code] [varchar](50) NULL CONSTRAINT [DF__BussinessG__Code__797309D9]  DEFAULT (NULL),
	[Name] [nvarchar](250) NOT NULL,
	[Status] [bit] NULL CONSTRAINT [DF__Bussiness__Statu__7A672E12]  DEFAULT (NULL),
	[Note] [nvarchar](500) NULL CONSTRAINT [DF__BussinessG__Note__7B5B524B]  DEFAULT (NULL),
	[CreatedBy] [varchar](150) NULL CONSTRAINT [DF__Bussiness__Creat__7C4F7684]  DEFAULT (NULL),
	[CreatedAt] [datetime] NULL CONSTRAINT [DF__Bussiness__Creat__7D439ABD]  DEFAULT (NULL),
	[ModifiedBy] [varchar](150) NULL CONSTRAINT [DF__Bussiness__Modif__7E37BEF6]  DEFAULT (NULL),
	[ModifiedAt] [datetime] NULL CONSTRAINT [DF__Bussiness__Modif__7F2BE32F]  DEFAULT (NULL),
 CONSTRAINT [PK__Bussines__3214EC07D41C2DCE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[F_CSKH.05]    Script Date: 4/26/2021 4:10:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[F_CSKH.05](
	[Id] [varchar](150) NOT NULL,
	[CompanyCode] [varchar](150) NULL,
	[MAKY] [varchar](150) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreateUser] [nvarchar](250) NULL,
	[UpdateUser] [nvarchar](250) NULL,
	[CHITIEUD] [nvarchar](250) NULL,
	[TUONGQUAN] [float] NULL,
	[TRONGSO] [float] NULL,
	[idUser] [varchar](150) NULL,
	[version] [int] NULL,
 CONSTRAINT [PK_F_CSKH.05] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[F_CSKH05_TRASH]    Script Date: 4/26/2021 4:10:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[F_CSKH05_TRASH](
	[Id] [varchar](150) NOT NULL,
	[CompanyCode] [varchar](150) NULL,
	[MAKY] [varchar](150) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreateUser] [nvarchar](250) NULL,
	[UpdateUser] [nvarchar](250) NULL,
	[CHITIEUD] [nvarchar](250) NULL,
	[TUONGQUAN] [float] NULL,
	[TRONGSO] [float] NULL,
	[idUser] [varchar](150) NULL,
	[version] [int] NULL,
 CONSTRAINT [PK_F_CSKH05_TRASH] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GroupUser]    Script Date: 4/26/2021 4:10:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GroupUser](
	[Id] [varchar](150) NOT NULL,
	[Code] [varchar](150) NULL,
	[Name] [nvarchar](500) NULL,
	[Status] [bit] NULL,
	[CreatedBy] [varchar](150) NULL CONSTRAINT [DF_groupuser_CreatedBy]  DEFAULT (NULL),
	[CreatedAt] [datetime] NULL CONSTRAINT [DF_groupuser_CreatedAt]  DEFAULT (NULL),
	[ModifiedBy] [varchar](150) NULL CONSTRAINT [DF_groupuser_ModifiedBy]  DEFAULT (NULL),
	[ModifiedAt] [datetime] NULL CONSTRAINT [DF_groupuser_ModifiedAt]  DEFAULT (NULL),
 CONSTRAINT [PK_groupuser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Region]    Script Date: 4/26/2021 4:10:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Region](
	[Id] [varchar](32) NOT NULL,
	[AreaId] [varchar](32) NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[Name] [nvarchar](250) NULL CONSTRAINT [DF__Region__Name__2BFE89A6]  DEFAULT (NULL),
	[RegionLevel] [int] NULL CONSTRAINT [DF__Region__RegionLe__2CF2ADDF]  DEFAULT (NULL),
	[RegionPath] [varchar](500) NULL CONSTRAINT [DF__Region__RegionPa__2DE6D218]  DEFAULT (NULL),
	[ParentId] [varchar](32) NULL CONSTRAINT [DF__Region__ParentId__2EDAF651]  DEFAULT (NULL),
	[BiCode] [varchar](255) NULL CONSTRAINT [DF__Region__BiCode__2FCF1A8A]  DEFAULT (NULL),
	[CreatedBy] [varchar](150) NULL CONSTRAINT [DF__Region__CreatedB__30C33EC3]  DEFAULT (NULL),
	[CreatedAt] [datetime] NULL CONSTRAINT [DF__Region__CreatedA__31B762FC]  DEFAULT (NULL),
	[ModifiedBy] [varchar](150) NULL CONSTRAINT [DF__Region__Modified__32AB8735]  DEFAULT (NULL),
	[ModifiedAt] [datetime] NULL CONSTRAINT [DF__Region__Modified__339FAB6E]  DEFAULT (NULL),
	[Status] [bit] NULL,
 CONSTRAINT [PK__Region__3214EC07AFF0CFA0] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Role]    Script Date: 4/26/2021 4:10:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Role](
	[Id] [varchar](32) NOT NULL,
	[Code] [varchar](50) NULL DEFAULT (NULL),
	[Name] [nvarchar](255) NULL DEFAULT (NULL),
	[IsActive] [bit] NULL DEFAULT (NULL),
	[CreatedBy] [varchar](32) NULL DEFAULT (NULL),
	[CreatedAt] [datetime] NULL DEFAULT (NULL),
	[ModifiedBy] [varchar](32) NULL DEFAULT (NULL),
	[ModifiedAt] [datetime] NULL DEFAULT (NULL),
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RoleDetail]    Script Date: 4/26/2021 4:10:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RoleDetail](
	[Id] [varchar](32) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [nvarchar](255) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [varchar](32) NULL,
	[CreatedAt] [datetime] NULL,
	[ModifiedBy] [varchar](32) NULL,
	[ModifiedAt] [datetime] NULL,
 CONSTRAINT [PK_RoleDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RoleRoleDetail]    Script Date: 4/26/2021 4:10:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RoleRoleDetail](
	[Id] [varchar](32) NOT NULL,
	[RoleId] [varchar](32) NULL,
	[RoleDetailId] [varchar](32) NULL,
 CONSTRAINT [PK_RoleRoleDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 4/26/2021 4:10:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[Id] [varchar](150) NOT NULL,
	[Title] [varchar](10) NULL CONSTRAINT [DF__user__Title__0B91BA14]  DEFAULT (NULL),
	[UserName] [varchar](255) NULL CONSTRAINT [DF__user__UserName__0C85DE4D]  DEFAULT (NULL),
	[Email] [varchar](255) NULL CONSTRAINT [DF__user__Email__0D7A0286]  DEFAULT (NULL),
	[FirstName] [varchar](255) NULL CONSTRAINT [DF__user__FirstName__0E6E26BF]  DEFAULT (NULL),
	[MidName] [varchar](255) NULL CONSTRAINT [DF__user__MidName__0F624AF8]  DEFAULT (NULL),
	[LastName] [varchar](255) NULL CONSTRAINT [DF__user__LastName__10566F31]  DEFAULT (NULL),
	[FullName] [nvarchar](255) NULL CONSTRAINT [DF__user__FullName__114A936A]  DEFAULT (NULL),
	[GroupId] [int] NULL CONSTRAINT [DF__user__GroupId__123EB7A3]  DEFAULT (NULL),
	[Status] [int] NULL CONSTRAINT [DF__user__Status__1332DBDC]  DEFAULT (NULL),
	[Note] [nvarchar](500) NULL,
	[RefreshToken] [varchar](3000) NULL CONSTRAINT [DF__user__RefreshTok__14270015]  DEFAULT (NULL),
	[Groups] [varchar](255) NULL CONSTRAINT [DF__user__Groups__151B244E]  DEFAULT (NULL),
	[ReceiveEmail] [varchar](45) NULL CONSTRAINT [DF__user__ReceiveEma__160F4887]  DEFAULT (NULL),
	[Mobile] [varchar](25) NULL CONSTRAINT [DF__user__Mobile__17036CC0]  DEFAULT (NULL),
	[Tel] [varchar](25) NULL CONSTRAINT [DF__user__Tel__17F790F9]  DEFAULT (NULL),
	[OtherEmail] [varchar](255) NULL CONSTRAINT [DF__user__OtherEmail__18EBB532]  DEFAULT (NULL),
	[Avatar] [varchar](2000) NULL CONSTRAINT [DF__user__Avatar__19DFD96B]  DEFAULT (NULL),
	[Language] [varchar](10) NULL CONSTRAINT [DF__user__Language__1AD3FDA4]  DEFAULT (NULL),
	[LastLoginAt] [datetime] NULL CONSTRAINT [DF__user__LastLoginA__1BC821DD]  DEFAULT (NULL),
	[CreatedBy] [varchar](150) NULL CONSTRAINT [DF__user__CreatedBy__1CBC4616]  DEFAULT (NULL),
	[CreatedAt] [datetime] NULL CONSTRAINT [DF__user__CreatedAt__1DB06A4F]  DEFAULT (NULL),
	[ModifiedBy] [varchar](150) NULL CONSTRAINT [DF__user__ModifiedBy__1EA48E88]  DEFAULT (NULL),
	[ModifiedAt] [datetime] NULL CONSTRAINT [DF__user__ModifiedAt__1F98B2C1]  DEFAULT (NULL),
	[IsAdmin] [bit] NULL,
	[GroupBireportId] [varchar](150) NULL,
	[PassWord] [varchar](50) NULL,
 CONSTRAINT [PK__user__3214EC07F7327CD3] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserBussinessGroup]    Script Date: 4/26/2021 4:10:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserBussinessGroup](
	[Id] [varchar](150) NOT NULL,
	[UserId] [varchar](150) NULL,
	[BussinessGroupId] [varchar](150) NULL,
 CONSTRAINT [PK_UserBussinessGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserGroupUser]    Script Date: 4/26/2021 4:10:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserGroupUser](
	[Id] [varchar](150) NOT NULL,
	[UserId] [varchar](150) NOT NULL,
	[GroupUserId] [varchar](150) NOT NULL,
 CONSTRAINT [PK_UserGroupUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 4/26/2021 4:10:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserRole](
	[Id] [varchar](150) NOT NULL,
	[UserId] [varchar](150) NOT NULL,
	[RoleId] [varchar](32) NOT NULL,
 CONSTRAINT [PK__UserRole__3214EC0785C0BF32] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[BussinessGroup] ([Id], [Code], [Name], [Status], [Note], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt]) VALUES (N'1bbe4809bac5443d82ebbe5ec93914ca', N'TTTH', N'Truyền thông thương hiệu', 0, NULL, NULL, NULL, N'cdadd8aa-b72d-416a-912f-b59a2735c165', CAST(N'2021-01-12 01:17:14.477' AS DateTime))
INSERT [dbo].[BussinessGroup] ([Id], [Code], [Name], [Status], [Note], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt]) VALUES (N'6158f9bf865f42dba520cf81bce0f383', N'KHCL', N'Kế hoạch', 1, NULL, NULL, NULL, N'cdadd8aa-b72d-416a-912f-b59a2735c165', CAST(N'2021-01-12 02:03:24.910' AS DateTime))
INSERT [dbo].[BussinessGroup] ([Id], [Code], [Name], [Status], [Note], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt]) VALUES (N'7749b875b03c4084a2a1921171db91dc', N'PTDL_CSKH', N'PTDL & CSKH', 1, NULL, NULL, NULL, N'cdadd8aa-b72d-416a-912f-b59a2735c165', CAST(N'2021-01-12 09:49:29.283' AS DateTime))
INSERT [dbo].[BussinessGroup] ([Id], [Code], [Name], [Status], [Note], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt]) VALUES (N'7968f824d8624313abe2a7b13b127431', N'BH-MKT', N'Bán hàng', 1, NULL, NULL, NULL, N'cdadd8aa-b72d-416a-912f-b59a2735c165', CAST(N'2021-01-12 02:03:48.167' AS DateTime))
INSERT [dbo].[BussinessGroup] ([Id], [Code], [Name], [Status], [Note], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt]) VALUES (N'92dca439071e434db11452b3cefddc57', N'DV', N'Dịch vụ', 1, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BussinessGroup] ([Id], [Code], [Name], [Status], [Note], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt]) VALUES (N'b18f7149f7df4c86b72e64c584e8fcc4', N'VPDD', N'Văn phòng đại diện HCM', 0, NULL, NULL, NULL, N'cdadd8aa-b72d-416a-912f-b59a2735c165', CAST(N'2021-01-12 01:17:20.810' AS DateTime))
INSERT [dbo].[BussinessGroup] ([Id], [Code], [Name], [Status], [Note], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt]) VALUES (N'cf1548c0d42f4345b8ce768d1f20c8e4', N'TCKT', N'Tài chính kế toán', 0, NULL, NULL, NULL, N'cdadd8aa-b72d-416a-912f-b59a2735c165', CAST(N'2020-12-17 09:07:01.560' AS DateTime))
INSERT [dbo].[BussinessGroup] ([Id], [Code], [Name], [Status], [Note], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt]) VALUES (N'd449079ebda449829dfe9887da2fc292', N'LD', N'Lãnh đạo', 0, NULL, NULL, NULL, N'cdadd8aa-b72d-416a-912f-b59a2735c165', CAST(N'2021-01-12 01:18:34.660' AS DateTime))
INSERT [dbo].[BussinessGroup] ([Id], [Code], [Name], [Status], [Note], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt]) VALUES (N'fad80121be254063acdde6cbd560c587', N'HCNS', N'Hành chính nhân sự', 1, NULL, NULL, NULL, N'cdadd8aa-b72d-416a-912f-b59a2735c165', CAST(N'2020-12-17 10:00:47.257' AS DateTime))
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'140', N'Novaon', N'2021T1', CAST(N'2021-04-19 09:11:19.917' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr1', N'ud1', N'ct1', 1, 1, NULL, 1)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'141', N'Novaon', N'2021T1', CAST(N'2021-04-19 09:11:19.920' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr2', N'ud2', N'ct2', 1, 1, NULL, 1)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'142', N'Novaon', N'2021T1', CAST(N'2021-04-19 09:11:19.920' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr3', N'ud3', N'ct3', 1, 1, NULL, 1)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'143', N'Novaon', N'2021T1', CAST(N'2021-04-19 09:11:19.920' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr4', N'ud4', N'ct4', 1, 1, NULL, 1)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'144', N'Novaon', N'2021T1', CAST(N'2021-04-19 09:11:19.923' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr5', N'ud5', N'ct5', 1, 1, NULL, 1)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'145', N'Novaon', N'2021T1', CAST(N'2021-04-19 09:11:19.923' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr6', N'ud6', N'ct6', 1, 1, NULL, 1)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'146', N'Novaon', N'2021T1', CAST(N'2021-04-19 09:11:19.923' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr7', N'ud7', N'ct7', 1, 1, NULL, 1)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'147', N'Novaon', N'2021T1', CAST(N'2021-04-19 09:11:19.927' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr8', N'ud8', N'ct8', 1, 1, NULL, 1)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'148', N'Novaon', N'2021T1', CAST(N'2021-04-19 09:11:19.927' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr9', N'ud9', N'ct9', 1, 1, NULL, 1)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'149', N'Novaon', N'2021T1', CAST(N'2021-04-19 09:11:19.927' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr10', N'ud10', N'ct10', 1, 1, NULL, 2)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'150', N'Novaon', N'2021T2', CAST(N'2021-04-19 09:11:19.927' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr11', N'ud11', N'ct11', 1, 1, NULL, 2)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'151', N'Novaon', N'2021T2', CAST(N'2021-04-19 09:11:19.930' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr12', N'ud12', N'ct12', 1, 1, NULL, 2)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'152', N'Novaon', N'2021T1', CAST(N'2021-04-19 09:11:19.930' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr13', N'ud13', N'ct13', 1, 1, NULL, 2)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'153', N'Novaon', N'2021T2', CAST(N'2021-04-19 09:11:19.930' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr14', N'ud14', N'ct14', 1, 1, NULL, 2)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'154', N'Novaon', N'2021T2', CAST(N'2021-04-19 09:11:19.930' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr15', N'ud15', N'ct15', 1, 1, NULL, 2)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'155', N'Novaon', N'2021T2', CAST(N'2021-04-19 09:11:19.933' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr16', N'ud16', N'ct16', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'156', N'Novaon', N'2021T2', CAST(N'2021-04-19 09:11:19.933' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr17', N'ud17', N'ct17', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'157', N'Novaon', N'2021T2', CAST(N'2021-04-19 09:11:19.937' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr18', N'ud18', N'ct18', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'158', N'Novaon', N'2021T2', CAST(N'2021-04-19 09:11:19.937' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr19', N'ud19', N'ct19', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'159', N'Novaon', N'2021T2', CAST(N'2021-04-19 09:11:19.937' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr20', N'ud20', N'ct20', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'160', N'Novaon', N'ky143', CAST(N'2021-04-19 09:11:19.937' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr21', N'ud21', N'ct21', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'161', N'Novaon', N'ky144', CAST(N'2021-04-19 09:11:19.940' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr22', N'ud22', N'ct22', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'162', N'Novaon', N'ky145', CAST(N'2021-04-19 09:11:19.940' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr23', N'ud23', N'ct23', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'163', N'Novaon', N'ky146', CAST(N'2021-04-19 09:11:19.940' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr24', N'ud24', N'ct24', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'164', N'Novaon', N'ky147', CAST(N'2021-04-19 09:11:19.943' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr25', N'ud25', N'ct25', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'165', N'Novaon', N'ky148', CAST(N'2021-04-19 09:11:19.947' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr26', N'ud26', N'ct26', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'166', N'Novaon', N'ky149', CAST(N'2021-04-19 09:11:19.947' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr27', N'ud27', N'ct27', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'167', N'Novaon', N'ky150', CAST(N'2021-04-19 09:11:19.947' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr28', N'ud28', N'ct28', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'168', N'Novaon', N'ky151', CAST(N'2021-04-19 09:11:19.947' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr29', N'ud29', N'ct29', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'169', N'Novaon', N'ky152', CAST(N'2021-04-19 09:11:19.950' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr30', N'ud30', N'ct30', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'170', N'Novaon', N'ky153', CAST(N'2021-04-19 09:11:19.950' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr31', N'ud31', N'ct31', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'171', N'Novaon', N'ky154', CAST(N'2021-04-19 09:11:19.953' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr32', N'ud32', N'ct32', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'172', N'Novaon', N'ky155', CAST(N'2021-04-19 09:11:19.953' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr33', N'ud33', N'ct33', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'173', N'Novaon', N'ky156', CAST(N'2021-04-19 09:11:19.953' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr34', N'ud34', N'ct34', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'174', N'Novaon', N'ky157', CAST(N'2021-04-19 09:11:19.957' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr35', N'ud35', N'ct35', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'175', N'Novaon', N'ky158', CAST(N'2021-04-19 09:11:19.957' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr36', N'ud36', N'ct36', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'176', N'Novaon', N'ky159', CAST(N'2021-04-19 09:11:19.957' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr37', N'ud37', N'ct37', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'177', N'Novaon', N'ky160', CAST(N'2021-04-19 09:11:19.957' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr38', N'ud38', N'ct38', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'178', N'Novaon', N'ky161', CAST(N'2021-04-19 09:11:19.960' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr39', N'ud39', N'ct39', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'179', N'Novaon', N'ky162', CAST(N'2021-04-19 09:11:19.960' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr40', N'ud40', N'ct40', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'180', N'Novaon', N'ky163', CAST(N'2021-04-19 09:11:19.963' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr41', N'ud41', N'ct41', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'181', N'Novaon', N'ky164', CAST(N'2021-04-19 09:11:19.963' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr42', N'ud42', N'ct42', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'182', N'Novaon', N'ky165', CAST(N'2021-04-19 09:11:19.967' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr43', N'ud43', N'ct43', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'183', N'Novaon', N'ky166', CAST(N'2021-04-19 09:11:19.967' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr44', N'ud44', N'ct44', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'184', N'Novaon', N'ky167', CAST(N'2021-04-19 09:11:19.967' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr45', N'ud45', N'ct45', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'185', N'Novaon', N'ky168', CAST(N'2021-04-19 09:11:19.967' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr46', N'ud46', N'ct46', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'186', N'Novaon', N'ky169', CAST(N'2021-04-19 09:11:19.970' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr47', N'ud47', N'ct47', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'187', N'Novaon', N'ky170', CAST(N'2021-04-19 09:11:19.970' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr48', N'ud48', N'ct48', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'188', N'Novaon', N'ky171', CAST(N'2021-04-19 09:11:19.970' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr49', N'ud49', N'ct49', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'189', N'Novaon', N'ky172', CAST(N'2021-04-19 09:11:19.973' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr50', N'ud50', N'ct50', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'190', N'Novaon', N'ky173', CAST(N'2021-04-19 09:11:19.973' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr51', N'ud51', N'ct51', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'191', N'Novaon', N'ky174', CAST(N'2021-04-19 09:11:19.973' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr52', N'ud52', N'ct52', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'192', N'Novaon', N'ky175', CAST(N'2021-04-19 09:11:19.977' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr53', N'ud53', N'ct53', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'193', N'Novaon', N'ky176', CAST(N'2021-04-19 09:11:19.977' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr54', N'ud54', N'ct54', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'194', N'Novaon', N'ky177', CAST(N'2021-04-19 09:11:19.980' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr55', N'ud55', N'ct55', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'195', N'Novaon', N'ky178', CAST(N'2021-04-19 09:11:19.980' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr56', N'ud56', N'ct56', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'196', N'Novaon', N'ky179', CAST(N'2021-04-19 09:11:19.980' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr57', N'ud57', N'ct57', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'197', N'Novaon', N'ky180', CAST(N'2021-04-19 09:11:19.983' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr58', N'ud58', N'ct58', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'198', N'Novaon', N'ky181', CAST(N'2021-04-19 09:11:19.983' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr59', N'ud59', N'ct59', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'199', N'Novaon', N'ky182', CAST(N'2021-04-19 09:11:19.987' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr60', N'ud60', N'ct60', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'200', N'Novaon', N'ky183', CAST(N'2021-04-19 09:11:19.987' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr61', N'ud61', N'ct61', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'201', N'Novaon', N'ky184', CAST(N'2021-04-19 09:11:19.987' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr62', N'ud62', N'ct62', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'202', N'Novaon', N'ky185', CAST(N'2021-04-19 09:11:19.990' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr63', N'ud63', N'ct63', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'203', N'Novaon', N'ky186', CAST(N'2021-04-19 09:11:19.990' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr64', N'ud64', N'ct64', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'204', N'Novaon', N'ky187', CAST(N'2021-04-19 09:11:19.993' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr65', N'ud65', N'ct65', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'205', N'Novaon', N'ky188', CAST(N'2021-04-19 09:11:19.993' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr66', N'ud66', N'ct66', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'206', N'Novaon', N'ky189', CAST(N'2021-04-19 09:11:19.993' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr67', N'ud67', N'ct67', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'207', N'Novaon', N'ky190', CAST(N'2021-04-19 09:11:19.997' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr68', N'ud68', N'ct68', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'208', N'Novaon', N'ky191', CAST(N'2021-04-19 09:11:19.997' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr69', N'ud69', N'ct69', 1, 1, NULL, 3)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'209', N'Novaon', N'ky123', CAST(N'2021-04-23 02:18:15.563' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr1', N'ud1', N'ct1', 1, 1, NULL, 4)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'300', N'Novaon', N'ky123', CAST(N'2021-04-26 09:30:15.257' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr1', N'ud1', N'ct1', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'301', N'Novaon', N'ky124', CAST(N'2021-04-26 09:30:15.337' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr2', N'ud2', N'ct2', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'302', N'Novaon', N'ky125', CAST(N'2021-04-26 09:30:15.350' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr3', N'ud3', N'ct3', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'303', N'Novaon', N'ky126', CAST(N'2021-04-26 09:30:15.353' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr4', N'ud4', N'ct4', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'304', N'Novaon', N'ky127', CAST(N'2021-04-26 09:30:15.353' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr5', N'ud5', N'ct5', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'305', N'Novaon', N'ky128', CAST(N'2021-04-26 09:30:15.357' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr6', N'ud6', N'ct6', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'306', N'Novaon', N'ky129', CAST(N'2021-04-26 09:30:15.357' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr7', N'ud7', N'ct7', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'307', N'Novaon', N'ky130', CAST(N'2021-04-26 09:30:15.357' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr8', N'ud8', N'ct8', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'308', N'Novaon', N'ky131', CAST(N'2021-04-26 09:30:15.357' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr9', N'ud9', N'ct9', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'309', N'Novaon', N'ky132', CAST(N'2021-04-26 09:30:15.360' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr10', N'ud10', N'ct10', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'310', N'Novaon', N'ky133', CAST(N'2021-04-26 09:30:15.360' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr11', N'ud11', N'ct11', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'311', N'Novaon', N'ky134', CAST(N'2021-04-26 09:30:15.360' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr12', N'ud12', N'ct12', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'312', N'Novaon', N'ky135', CAST(N'2021-04-26 09:30:15.363' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr13', N'ud13', N'ct13', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'313', N'Novaon', N'ky136', CAST(N'2021-04-26 09:30:15.363' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr14', N'ud14', N'ct14', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'314', N'Novaon', N'ky137', CAST(N'2021-04-26 09:30:15.363' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr15', N'ud15', N'ct15', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'315', N'Novaon', N'ky138', CAST(N'2021-04-26 09:30:15.363' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr16', N'ud16', N'ct16', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'316', N'Novaon', N'ky139', CAST(N'2021-04-26 09:30:15.367' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr17', N'ud17', N'ct17', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'317', N'Novaon', N'ky140', CAST(N'2021-04-26 09:30:15.367' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr18', N'ud18', N'ct18', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'318', N'Novaon', N'ky141', CAST(N'2021-04-26 09:30:15.367' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr19', N'ud19', N'ct19', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'319', N'Novaon', N'ky142', CAST(N'2021-04-26 09:30:15.367' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr20', N'ud20', N'ct20', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'320', N'Novaon', N'ky143', CAST(N'2021-04-26 09:30:15.370' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr21', N'ud21', N'ct21', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'321', N'Novaon', N'ky144', CAST(N'2021-04-26 09:30:15.370' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr22', N'ud22', N'ct22', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'322', N'Novaon', N'ky145', CAST(N'2021-04-26 09:30:15.370' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr23', N'ud23', N'ct23', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'323', N'Novaon', N'ky146', CAST(N'2021-04-26 09:30:15.373' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr24', N'ud24', N'ct24', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'324', N'Novaon', N'ky147', CAST(N'2021-04-26 09:30:15.373' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr25', N'ud25', N'ct25', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'325', N'Novaon', N'ky148', CAST(N'2021-04-26 09:30:15.373' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr26', N'ud26', N'ct26', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'326', N'Novaon', N'ky149', CAST(N'2021-04-26 09:30:15.373' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr27', N'ud27', N'ct27', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'327', N'Novaon', N'ky150', CAST(N'2021-04-26 09:30:15.377' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr28', N'ud28', N'ct28', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'328', N'Novaon', N'ky151', CAST(N'2021-04-26 09:30:15.377' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr29', N'ud29', N'ct29', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'329', N'Novaon', N'ky152', CAST(N'2021-04-26 09:30:15.377' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr30', N'ud30', N'ct30', 1, 1, NULL, NULL)
GO
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'330', N'Novaon', N'ky153', CAST(N'2021-04-26 09:30:15.377' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr31', N'ud31', N'ct31', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'331', N'Novaon', N'ky154', CAST(N'2021-04-26 09:30:15.380' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr32', N'ud32', N'ct32', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'332', N'Novaon', N'ky155', CAST(N'2021-04-26 09:30:15.380' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr33', N'ud33', N'ct33', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'333', N'Novaon', N'ky156', CAST(N'2021-04-26 09:30:15.380' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr34', N'ud34', N'ct34', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'334', N'Novaon', N'ky157', CAST(N'2021-04-26 09:30:15.383' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr35', N'ud35', N'ct35', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'335', N'Novaon', N'ky158', CAST(N'2021-04-26 09:30:15.383' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr36', N'ud36', N'ct36', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'336', N'Novaon', N'ky159', CAST(N'2021-04-26 09:30:15.383' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr37', N'ud37', N'ct37', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'337', N'Novaon', N'ky160', CAST(N'2021-04-26 09:30:15.387' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr38', N'ud38', N'ct38', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'338', N'Novaon', N'ky161', CAST(N'2021-04-26 09:30:15.387' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr39', N'ud39', N'ct39', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'339', N'Novaon', N'ky162', CAST(N'2021-04-26 09:30:15.387' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr40', N'ud40', N'ct40', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'340', N'Novaon', N'ky163', CAST(N'2021-04-26 09:30:15.387' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr41', N'ud41', N'ct41', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'341', N'Novaon', N'ky164', CAST(N'2021-04-26 09:30:15.390' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr42', N'ud42', N'ct42', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'342', N'Novaon', N'ky165', CAST(N'2021-04-26 09:30:15.390' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr43', N'ud43', N'ct43', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'343', N'Novaon', N'ky166', CAST(N'2021-04-26 09:30:15.390' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr44', N'ud44', N'ct44', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'344', N'Novaon', N'ky167', CAST(N'2021-04-26 09:30:15.393' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr45', N'ud45', N'ct45', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'345', N'Novaon', N'ky168', CAST(N'2021-04-26 09:30:15.393' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr46', N'ud46', N'ct46', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'346', N'Novaon', N'ky169', CAST(N'2021-04-26 09:30:15.393' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr47', N'ud47', N'ct47', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'347', N'Novaon', N'ky170', CAST(N'2021-04-26 09:30:15.397' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr48', N'ud48', N'ct48', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'348', N'Novaon', N'ky171', CAST(N'2021-04-26 09:30:15.397' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr49', N'ud49', N'ct49', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'349', N'Novaon', N'ky172', CAST(N'2021-04-26 09:30:15.397' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr50', N'ud50', N'ct50', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'350', N'Novaon', N'ky173', CAST(N'2021-04-26 09:30:15.397' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr51', N'ud51', N'ct51', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'351', N'Novaon', N'ky174', CAST(N'2021-04-26 09:30:15.400' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr52', N'ud52', N'ct52', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'352', N'Novaon', N'ky175', CAST(N'2021-04-26 09:30:15.400' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr53', N'ud53', N'ct53', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'353', N'Novaon', N'ky176', CAST(N'2021-04-26 09:30:15.400' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr54', N'ud54', N'ct54', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'354', N'Novaon', N'ky177', CAST(N'2021-04-26 09:30:15.403' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr55', N'ud55', N'ct55', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'355', N'Novaon', N'ky178', CAST(N'2021-04-26 09:30:15.403' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr56', N'ud56', N'ct56', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'356', N'Novaon', N'ky179', CAST(N'2021-04-26 09:30:15.403' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr57', N'ud57', N'ct57', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'357', N'Novaon', N'ky180', CAST(N'2021-04-26 09:30:15.407' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr58', N'ud58', N'ct58', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'358', N'Novaon', N'ky181', CAST(N'2021-04-26 09:30:15.407' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr59', N'ud59', N'ct59', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'359', N'Novaon', N'ky182', CAST(N'2021-04-26 09:30:15.407' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr60', N'ud60', N'ct60', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'360', N'Novaon', N'ky183', CAST(N'2021-04-26 09:30:15.410' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr61', N'ud61', N'ct61', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'361', N'Novaon', N'ky184', CAST(N'2021-04-26 09:30:15.410' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr62', N'ud62', N'ct62', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'362', N'Novaon', N'ky185', CAST(N'2021-04-26 09:30:15.410' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr63', N'ud63', N'ct63', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'363', N'Novaon', N'ky186', CAST(N'2021-04-26 09:30:15.413' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr64', N'ud64', N'ct64', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'364', N'Novaon', N'ky187', CAST(N'2021-04-26 09:30:15.413' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr65', N'ud65', N'ct65', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'365', N'Novaon', N'ky188', CAST(N'2021-04-26 09:30:15.413' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr66', N'ud66', N'ct66', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'366', N'Novaon', N'ky189', CAST(N'2021-04-26 09:30:15.417' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr67', N'ud67', N'ct67', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'367', N'Novaon', N'ky190', CAST(N'2021-04-26 09:30:15.417' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr68', N'ud68', N'ct68', 1, 1, NULL, NULL)
INSERT [dbo].[F_CSKH.05] ([Id], [CompanyCode], [MAKY], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser], [CHITIEUD], [TUONGQUAN], [TRONGSO], [idUser], [version]) VALUES (N'368', N'Novaon', N'ky191', CAST(N'2021-04-26 09:30:15.417' AS DateTime), CAST(N'1999-02-02 00:00:00.000' AS DateTime), N'cr69', N'ud69', N'ct69', 1, 1, NULL, NULL)
INSERT [dbo].[GroupUser] ([Id], [Code], [Name], [Status], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt]) VALUES (N'082832ac-ae3c-4908-8cc4-c5e6df4773c7', N'View', N'Viewer User', 1, N'cdadd8aa-b72d-416a-912f-b59a2735c165', CAST(N'2021-01-12 13:25:48.543' AS DateTime), NULL, NULL)
INSERT [dbo].[GroupUser] ([Id], [Code], [Name], [Status], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt]) VALUES (N'1ea8cdde55034d85860218e32a76fbc7', N'323654', N'Nhóm Admin', 1, NULL, NULL, N'cdadd8aa-b72d-416a-912f-b59a2735c165', CAST(N'2020-12-25 16:18:59.623' AS DateTime))
INSERT [dbo].[GroupUser] ([Id], [Code], [Name], [Status], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt]) VALUES (N'td127d08daa94b1fb9a6031b1f482f8t', N'TuyenDQ', N'Nhóm DQT', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Region] ([Id], [AreaId], [Code], [Name], [RegionLevel], [RegionPath], [ParentId], [BiCode], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [Status]) VALUES (N'2d61305a12751d0424e270a29f28ea90', N'b9b43f252b284b779f27ab728667f0e6', N'37', N'Nghệ An', 4, N'8ccad6c0371348a98f9b64365134f4f1.2d61305a12751d0424e270a29f28ea90.6639a420214a4f9ad6fa19c5eaaabb1b.2d61305a12751d0424e270a29f28ea90', N'6639a420214a4f9ad6fa19c5eaaabb1b', N'345345', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[Region] ([Id], [AreaId], [Code], [Name], [RegionLevel], [RegionPath], [ParentId], [BiCode], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [Status]) VALUES (N'6639a420214a4f9ad6fa19c5eaaabb1b', N'b9b43f252b284b779f27ab728667f0e6', N'72', N'Tuyên Quang', 3, N'8ccad6c0371348a98f9b64365134f4f1.2d61305a12751d0424e270a29f28ea90.6639a420214a4f9ad6fa19c5eaaabb1b', N'2d61305a12751d0424e270a29f28ea90', N'345345UY', NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[Region] ([Id], [AreaId], [Code], [Name], [RegionLevel], [RegionPath], [ParentId], [BiCode], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [Status]) VALUES (N'8ccad6c0371348a98f9b64365134f4f1', N'b9b43f252b284b779f27ab728667f0e6', N'38', N'Hà Tĩnh', 1, N'8ccad6c0371348a98f9b64365134f4f1', N'', N'KJASS8QT', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[Region] ([Id], [AreaId], [Code], [Name], [RegionLevel], [RegionPath], [ParentId], [BiCode], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [Status]) VALUES (N'a6be3df19e9691d0d747a86611b9d2bd', N'b9b43f252b284b779f27ab728667f0e6', N'30', N'Hà Nội', 2, N'2d61305a12751d0424e270a29f28ea90.a6be3df19e9691d0d747a86611b9d2bd', N'2d61305a12751d0424e270a29f28ea90', N'345345YES', NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[Region] ([Id], [AreaId], [Code], [Name], [RegionLevel], [RegionPath], [ParentId], [BiCode], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [Status]) VALUES (N'c6e790ecede04607b4de1e2c8e960e69', N'b9b43f252b284b779f27ab728667f0e6', N'32', N'Tuyên Quang', NULL, NULL, N'', N'345345', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[Role] ([Id], [Code], [Name], [IsActive], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt]) VALUES (N'1', N'SUPER_ADMIN', N'Manage all', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Role] ([Id], [Code], [Name], [IsActive], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt]) VALUES (N'5b86edcb09ec4453895bf55620605214', N'MANAGE_USER', N'Manage user', 1, NULL, NULL, N'654d1d7610b74398ab7c0edc506cb58f', CAST(N'2020-09-11 16:52:50.000' AS DateTime))
INSERT [dbo].[Role] ([Id], [Code], [Name], [IsActive], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt]) VALUES (N'6393d4f7330141fba8a5646868abe49c', N'MASTERDATA_ADMIN', N'Manage master data', 1, NULL, NULL, N'654d1d7610b74398ab7c0edc506cb58f', CAST(N'2020-10-02 11:32:32.000' AS DateTime))
INSERT [dbo].[RoleDetail] ([Id], [Code], [Name], [IsActive], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt]) VALUES (N'1', NULL, N'insert', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[RoleDetail] ([Id], [Code], [Name], [IsActive], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt]) VALUES (N'2', NULL, N'modify', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[RoleDetail] ([Id], [Code], [Name], [IsActive], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt]) VALUES (N'3', NULL, N'delete', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[RoleRoleDetail] ([Id], [RoleId], [RoleDetailId]) VALUES (N'1', N'5b86edcb09ec4453895bf55620605214', N'1')
INSERT [dbo].[RoleRoleDetail] ([Id], [RoleId], [RoleDetailId]) VALUES (N'2', N'6393d4f7330141fba8a5646868abe49c', N'1')
INSERT [dbo].[RoleRoleDetail] ([Id], [RoleId], [RoleDetailId]) VALUES (N'3', N'5b86edcb09ec4453895bf55620605214', N'2')
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'10764', N'Mr.', N'dangdatmta', N'anld6@fpt.com.vn', N'An', N'Ð?c', N'Lê', N'Đặng Quang Đạt', NULL, 1, NULL, NULL, NULL, NULL, N'0987264321', N'0987264321', NULL, NULL, NULL, CAST(N'2021-01-12 01:28:47.427' AS DateTime), NULL, NULL, N'424c41dfe29011ea8a8d0a580a820a95', CAST(N'2021-01-13 13:36:53.503' AS DateTime), NULL, N'f3adf14a-cea8-47c5-8876-76417200246e', N'123456')
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'108617', N'Ms.', N'buitramy', NULL, NULL, NULL, NULL, N'Bùi Thị Trà My', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'123456')
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'126974', N'rrr', N'rrr', N'dat', NULL, NULL, NULL, N'dat', NULL, 1, NULL, NULL, NULL, NULL, N'1232', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'127705', NULL, NULL, NULL, NULL, NULL, NULL, N'x', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'14734382802c4e5b85af56bdf7cc823a', N'Mr.', N'anld6', N'anld6@fpt.com.vn', N'An', N'Ð?c', N'Lê', N'Le Duc An (FIS ERP HN)', NULL, 1, NULL, NULL, NULL, NULL, N'0987264321', N'0987264321', NULL, NULL, NULL, CAST(N'2021-01-12 01:28:47.427' AS DateTime), NULL, NULL, N'424c41dfe29011ea8a8d0a580a820a95', CAST(N'2021-01-13 13:36:53.503' AS DateTime), NULL, N'f3adf14a-cea8-47c5-8876-76417200246e', NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'179427', N't', N't', N't', NULL, NULL, NULL, N'testttt', NULL, 0, NULL, NULL, NULL, NULL, N'1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'309886', N'ttttttttt', N'tttttt', N'1', NULL, NULL, NULL, N'tttttttt', NULL, 1, NULL, NULL, NULL, NULL, N'2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'31bb2f3b-b900-470a-823d-ae644768877d', NULL, N'trungvd', N'trungvd@htc.com.vn', NULL, NULL, NULL, N' Vu Dinh Trung (FIS ERP HN) ', NULL, 1, NULL, NULL, NULL, NULL, N'078097890', NULL, NULL, NULL, NULL, CAST(N'2021-01-13 14:20:19.687' AS DateTime), NULL, CAST(N'2021-01-13 13:20:43.603' AS DateTime), N'424c41dfe29011ea8a8d0a580a820a95', CAST(N'2021-01-13 14:00:08.100' AS DateTime), NULL, N'092063bf-e901-4855-aecf-28c5d016077c', NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'333594', N'ttttttttt', N'tttttt', N'1', NULL, NULL, NULL, N'tttttttt', NULL, 1, NULL, NULL, NULL, NULL, N'2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'338771', N'qqq', N'qqq', NULL, NULL, NULL, NULL, N'qqqqq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'383205', NULL, NULL, NULL, NULL, NULL, NULL, N'za', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'395148', N'Mr.', N'anld6', N'anld6@fpt.com.vn', N'An', N'Ð?c', N'Lê', N'Le Duc An (FIS ERP HN)', NULL, 1, NULL, NULL, NULL, NULL, N'0987264321', N'0987264321', NULL, NULL, NULL, CAST(N'2021-01-12 01:28:47.427' AS DateTime), NULL, NULL, N'424c41dfe29011ea8a8d0a580a820a95', CAST(N'2021-01-13 13:36:53.503' AS DateTime), NULL, N'f3adf14a-cea8-47c5-8876-76417200246e', NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'404673', N'Mr.', N'anld6', N'anld6@fpt.com.vn', N'An', N'Ð?c', N'Lê', N'Le Duc An (FIS ERP HN)', NULL, 1, NULL, NULL, NULL, NULL, N'0987264321', N'0987264321', NULL, NULL, NULL, CAST(N'2021-01-12 01:28:47.427' AS DateTime), NULL, NULL, N'424c41dfe29011ea8a8d0a580a820a95', CAST(N'2021-01-13 13:36:53.503' AS DateTime), NULL, N'f3adf14a-cea8-47c5-8876-76417200246e', NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'424c41dfe29011ea8a8d0a580a820a95', N'Mr.', N'datnq33', N'datnq33@fpt.com.vn', N'Ð?t', N'Quang', N'Nguy?n', N'Nguyen Quang Dat (FIS ERP HN)', NULL, 1, NULL, N'bcKLVvFp4JN6SpPw5GKarURVXaBwgFQQBbrePVpUzRcAAQAAM769l9LhF01oFXq9IkeFfEaeOKgZDWStB4qnwbeG20IZMJr26PZ1U9msZU1FhWg9gvEy0P_6Z3-L6KeEFqPyw6LkWxHOTh_2FdSgJaPFUSMNoZmts2kHHQX8jKJag7H-BRVcXrsgbsm6bRDYmnGe1gJnCFOBPZHnMXeS4oGJmDx9gIFQcwkTTmRoetUqcDoTwQ-LRktwKy6lhDAqrskeai1hwt4_B5y1OsLRFP1IMTM0SlZVNCxQB80YTc28P4UcFTNtlfE9dLisWtXEKf8f5J2UbSaiSdvY5u3-sAGZ7PIW66FiW560CvzAfMXwzzPMqUlERAoNiq9HL-syi345BNAFAAAyIx_QUnu7hAn1LxfXSnleTzoW8ko0vUqpf2Q5vNVPSRsJOgiwQlCLsvRchKyTnLGAN3FJKW5Xxc_SRl1IjwQVlKTOgAEogIwMRdXFYwDR57QJjW_rcS4uKyUh2dR7MIFoakT_PFzepzpgluLeEMSVc0g2vsSB6s2iDa5ODnBKcs6jquVNsGH0oGHR7UnZdX63eU4ZNg6nwfon2kxWTGeUhrqhDyyD7OW9gvyaMwG7l_iwQLzDvWCwqkbGFDdTrcBQMV80Qn6jFkU_lKWw30qdrRYRRayRvgVvc6gbBJpbyZ6c9xOS8uxTonFaTSYHQT73EH2Lbn65lq2iI2P4g0BSJlL2hYXbEOJfQdVPLujSwSST1aqRgc9I0Kadi8UgstYO3PMDvno8oBzeW6sw1YCK2TWQotBKSx-MUBl147VfG-FfNUtfDTM-BibZcwYjphBqrmy-G-O-f9MM4HwrfYjeMBmmfW-mANLhCSmLEBcUFXvonMvwOwTXJTFgSci_Us4sgsXtDKqf-t4f_eXnmZqS0m7J2Yy_VJLTS-pEs0jCiuS-FCjJQO47jW5vYrrS2LzI8TNOz-tyOTM3D3QHygCu801xMxz53hohPTiqr-06HNfCt3XqjTW1rYtbKqBIknisY6LUR7jylwnrowHDr0kxGRnY9xxO2yqX_oxglx_kn8eDVRtOh2Bmd97wMiufgQAh0im3LcxChP5T63dCx3KFQ8Hq1oCrVhzhcC4VTkLhvxLQyUhQdOK8VVxXpzBcwJzgMDStnUh8WGCuJ_44NO8WrK-WtvNQqY81KpCVlTxIVd18EJyQn34_aUAW31zkZ22MAorWl5BooXEI24hmAlH6GkJVTLOLZtYfPaGFCu1zOecIjazJX-yrklUXC0nMo2rMHEbgBwWTIZ4-Iks-rOCjWumJoYgZ8wrTZFHTVw0G0mnrB32zFKUUEMbPERdAoVj8WL9-6m0T2k2Z9WVuONaN3xx2yGN7CTuQuD93OMv_4DhQsaKnmGTxyHeGlQuQ9oPW6tSTvgEK90bxvJ28hnhrBJPiijc2eAwUFcgG1zAA4KkF5sGWdBgsazXxeVIrGfHCea7dB2hTq0ELcteMgD4Z1iSgE0fYYk17pHGhWoDFJNoha42i1PglUKcIdT0re0jT07de-u_GLa2pVWI7ekwC0yOxv8xGzfQzLF5UX3zOr0_27a09TJNGB8yhnQVVwlVgiv3PdvY0UORcnlq_D0dSiLQvbSjUp7EY9t9k-Zz8hAtXvx7eaeEXA8gdvHO32B4sN2O7M2RSijToJk7hoallXUqVmco6AELPnue8wMVXQm83OxI3bpID88WRnEE0BZU4W1PbL8-ugunptrYNK8Zs0N1l8D_pw1Ahcf2eHNJVUrGeL8GB1BNkr7_eeuipo8PMGwyS-uUSH2-AAYfRlnFmvgaftiM1mJCyMMjKlGGQmZZu_opz3EDz2mLHawAvJpas6sTdoNaUA81gZQfrxxvakuxpYMBCHU7McJfp5DWgUHfL7uuRXnmCNkeqnRAv4gS2nqhr42GCLvC1tozWV0C37o0pnqsmUUClXVFK9N9Kv5f5NeG0v4AZcQo3bqFgX0w2Il1T-qTxVtgP59EmKYCHuU0e4b-nHOeHLTELT7luhysL-YcmaV0KlaanF9c-uXO1Wz7CdiJUl-4Y2ujyRppMPiCsiCFB7PVYyNxq3h2_SHSd2XA2dCd4Ab4nuDIuL6fsfYH5AzWiq1E_JihrbXwG9QnX_l5vCYPQBTf7qDTG1yLctjxyFDLlSm6o5QctYPH61kNTfQUB_FAd8Ua17rIJvcmwh5zjX0418LFejGKtzKiEiTrPlz1qsBWUsGEk2JtqWyt-YjCtxKucWScPof82HVXOiwg0R0Xak_BunJVHjOtmGKfoTmVoIpahwFS_2AWXqk5FVtIyqOgOMgwWhZqPPQYaLocxM60KFVfo250IG47eaM8pr7FIzZx7ErIbMfs.BVnXtNf2GMwEhfuGU9d86FM0jv4QsQuBvjV085DsHk_i1cDUdv8ofN7jDcRVIdGIZPd736mEA5jQOQe0Pt89ckjUm5KyRFhtzNUaZvkcCjT6wat3EsemLV9-hlcpLPobpyCNxah9zzhxkhpePdBCBO7ItDmPE4iuVi2_auWEzwv_FAP2vmaqAaka1s8mkMC5CvYKGVPfYU3Ut0QnVOicnkLJtgWmTRsiDRIcaC8FoJdGAb80n94xhzoKnMfllOjNPX9j7gQo2OapSj2eUpdDwbhBj5v0rTYjMSEv-6f_VFdAnB4adXO7ULp6qaXj_cfgxip16RH3dPfTCX8nYVa8-w', NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2021-02-03 11:04:06.430' AS DateTime), NULL, NULL, N'cdadd8aa-b72d-416a-912f-b59a2735c165', CAST(N'2021-01-13 14:01:48.333' AS DateTime), NULL, N'092063bf-e901-4855-aecf-28c5d016077c', NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'5bb51a59045746eca861cd631bd2187d', N'Mr.', N'vunv27', N'vunv27@fpt.com.vn', N'Vu', N'Vi?t', N'Nguy?n', N'Nguyen Viet Vu (FIS ERP HN)', NULL, 0, NULL, N'bcKLVvFp4JN6SpPw5GKarURVXaBwgFQQBbrePVpUzRcAAQAADEmXIyXaxswcD4VogKbPaR00x__Pl8kCeiE1ZAVo4GGSH6_ALcucT9Aechpeh4P7u15ZsoYRHVi9S4AvKOl-QrNSDyGqUPh89RyMh8qhjrnWeZSkMJq-TXBVBZ9axCyxaHEGagjRs2Q--nYgW3cnSwht1PbJrYjzVGcUm0UQmi7BkURIN2O8TpmfltXv7ZbC8JeICSY2cnMe449Nn0YrSgMPeT8UBkilsa0F-bZGV6NkvbN7G6CnGm5PasqqV8dNWkcWEq2rz69qtGWlpkYUuJzgYpjJCF-t_KPdmgpmMOZSo6lC-cfwgH3954d_aB-6Dl9NT8WQMXj-3ME1CBU7irAFAAC13f4B2g7pOpb49UOOXMEkimXnpNHBaVd6yO_0dWqZJNLJx4x4BACAQz7oNqZuQifrKTyAteoDijR8sUXU6VQV3f4H9ZyzLOLIzu_-qMBzjAgBlNw7dM7KVL4UXJ6egrFNYvFlHUzAQRyJ6GTqespaX2B0ywUqZ0kqmLmotGCNc49MC1GnX17LBjfEnz16E-F7UHtPUWe6EdTj227GYRwPObumPEhyXouJjYLq2jMfXZ-JCp4J1i8XFw0pX0KVuE1AprS0tk7jHZAduuzjkqmFEIBPlsBTNBzVMOaneASF4NCWPIu85Xwn5YhSTRpW_9W8VdX2SlMiZd3sSw7WgeqqRPquVpGD4e3y-NFxkFBE70SEESwbaQ6pV5hEFgx6mFw6GOABJkta_GyG8wlnvorz8tqMjnpIiQF0dhAIzTHvL6zMGserL04XiLtV1Ny3CExM0eBNcxl06-XqhombsQGryLNcBC0FG9e2AeCRQUhlhJKghRNMxj5mDM5mhaaicGexRpWbKVcS0BUDhfFD4oLG22-RcJaIyoouuZ1hZ8J8w9QnZkZaoduMEB3A-h4ljBzszvZ0woT6pX8_ZjljuSD9X_Dq26tvd-XOvvz7qL-8NHTbfgqjvDGbhQbWPhqlXbwqUUDWqO4RseNIYfqbKMGX0N84Ar1VnGk341LipTjxaVnLnYymjCnwC00fjUOcC-BB3-Q3_h-jOiq-XQuFDOAwt2pkWoFlItpzAYZoDIdjnkY5IoIMexk5d-0MjpfmaV2R_JmTCP_YT3gB56idv2pVgH233x7cU6FwMXVWALGVya2_tJBneJdvW6faJ6mZO61bPyK3MnrPjiwELJFNdanm9v5R8QnDayyYcy5T5CumYX8QvyMpp_hD1D0xt-asovCyvLukpDqjhtUpA_CTF0kxA0ifPGBjwMuOXFRFBjvU3IRDUQYUwOn4EnNREfE5JFKytFeVPWchWb8yr9uFY0nq0L5fthHOlBsiCHsbv-qgWK63WKnj6Pwq1brwvMtxbn8-Z3V29gboePlJKR4sEnKvIUQ_xki0q2YXD8XehhhDJe4QeARA1Fy9SMKXrjfzHu0ADLzMauaKrgOOK8fec3udH8qtc8u9UQ8Sf0aC_cNW6JJLRihnvmN8PaEw_0y7EFV0_P8OYRxH5y5kBiSzqGpSqxbjkgC65ZF-c1caq3aRtghNUI9pdb-ec_99zM0DC8lvWva3MHdPm1TexTsK0zlgMYsP1RPtWxl-m41TYadNdaXD67YKduJaeuyqnxYuA7z8w1qsAUoTjMepDO4hXVz_syfQDnypV-iZk4tkDhS-3wXAgKsFFCMoQWaq6LGruSn_ZnYv2uggvEAWLKiEJJ9JdvzDfzxhd7y1MPeKrFUhy7lnwg2tPhCQriHEqY4sGDv9RLzrMRDZp3gjrfOo3gcsKytpj0ISgPvmYJsgHgyq9JFayOrJMr_1RL3X6sbpypYrRUmQlwWeDGJ_P5JBSGUxhIBowGGr5M_jDaz2d_a-Ql8MefzVTJl3L53qxf3vr7zlMYYd2rijQcNfxmphdVmJYS6iA9hXXnGqr7xL8AbnBnfZCW94lfLedgFxy_fnQmgeNTyP30o4nuCswp_10D5YrStryiR0lJlAa_tEG38GFqPLZ555ikmH16h9YaOnRPNiDE4yKoN4_WK2XAHrflpR1LS8mflPFDpGcVP2cu1rUuy_kTfEpwtg50-rkPloBVJ9sx8OcqDcVjOf0ByN0QJqUs-RLX39Ghk23dOlDlwhaImW87FDJbCZ3ydJsPSG6D91ljI6CPXu6UbVx_wieX8jkCO0K5mxUPE2VVpMf7WDD2wanGkRBQEX7ELYRFwH_qvSA3ZgW46u226f5sTDozTEy20SSY9QMFm9tAbsLs_eaOPI4txWf0rg18Jvay4jAc1Zus_C7OKRtQYsfBBJUHYd.fPoyJWftf3dOzFe_e4_TDc0bWbYD5h9RAhLRbmySHkCdOZtjRwkMB--TbsR5JTvwAbqVmEpOvMDOjONTWwd36Lzc7gWz03XBK4HYC7zSjNFhtL0RW21FhLzfq7y2ER76Z-CLsjphZlqA4kq-Lsly7ndUFAgCV-Sof1bR7WbEt-Ivvoqisc0FOShacrPCUl2H7SGcyfVphLB6zbj35T5FJOftwINH-9-6gHbZe61rkyrHWzaFziHTriy99vTD4ROUsSMfqi0r--Gja4w9GJ2eC-TFHEthU0Eu3ZGmNNUN8VCClwHPG1ZA9V0Kgoh2o6ggpYPx1h5PKpbi0ljB7BUjBA', NULL, NULL, N'0987654321', N'0987654321', NULL, NULL, NULL, CAST(N'2020-11-03 10:01:31.000' AS DateTime), N'654d1d7610b74398ab7c0edc506cb58f', CAST(N'2020-10-01 14:53:28.000' AS DateTime), N'424c41dfe29011ea8a8d0a580a820a95', CAST(N'2021-01-13 13:28:50.957' AS DateTime), NULL, N'a12833bd-7195-48fa-a7b1-9acb0bf67e87', NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'60996', NULL, NULL, NULL, NULL, NULL, NULL, N'xa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'610406', NULL, NULL, NULL, NULL, NULL, NULL, N'test1ne', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'630246', N'mr', N'datr', N'12@', NULL, NULL, NULL, N'dat', NULL, 1, NULL, NULL, NULL, NULL, N'123', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'654d1d7610b74398ab7c0edc506cb58f', N'Mr.', N'trungvd11', N'trungvd11@fpt.com.vn', N'Trung', N'Ðình', N'Vu', N'Vu Dinh Trung (FIS ERP HN)', NULL, 1, NULL, N'bcKLVvFp4JN6SpPw5GKarURVXaBwgFQQBbrePVpUzRcAAQAArCfekPnFl83xRqsYslusN4XHNqbGVla6lm8g7tpWRcHodEVFb0ZbVC7Gc8ikUv7SSJ-OiiWWha4UYV0qE_2luIuO55i92ZkUPZcoE2qgbpyjL5j4ZIghVbtQ_Mt0KeW85MlmtiQXryBOVtWNyvVTsqPOmbMeId-nh1FPTiVom-9rWgFd0ih0Yr-AtwMZLRkUFsjLngOa4mWc12HyX9g7r6f1G5Q7YdGHjDertHQ1ti_6VsooiVyZxwzyOjZ3k0c6C574mDiH-3TY9yYetPwH4WNhW0W3ADs23YWGF-qCIGDjKVJ9mH2QCDSs5y8s_IkFNAOg2z_m_A1uuR26t4izCZAFAABGeb7iK3-0v456Nzw1HFuFnJHqTXm9O5V31bIAiDLmo1RcZvlAVN4TZmnVyA5t_Pk81Uy4sz58iU6tUSv-3uJJRrKySSDozwm59ZZyyFbSJo9qeEiYGbuDoro4BVBGv_gsush3tKj2TivfcgyXbDoxH6bfIU0YiPbPVdUOIHBlZzwRPZd9bMEoctnAXkMCw0zLqUPX5AM04qlMY4PIE2FrZ2DJRCnJtfOpWVlYb16WsPtl-9LGsLswycF54LNSe2DlSDvc7KTTHkNbxi9gooxnFq1S0tiUU64Q7gxxh7YgvqMns9dogoT42tkrANvjRbPC-94eiVpKmPdzcPeP3oLKkXYWGSSp1WGzWN-BBZ4CH9xUz5mupBv3pSArVZEmfHpVXLezZKpYuTwYUzrnd17xuLYDRLwvvkHjyMB6LtTyAbUMj7zX17j33H_syRXdrvzo0kzYg3u_14ROr2wQybMffMBqJ_7_Apku0z9yl_v2L0MicwGneIBOMDxsbDkNJJ57fbEqYPiBaOzk8ZNnUVu4zVd4dpaw-qAmDGNre0F5oij5rLhEIvy6_q3axhZW2erMdTfSE6zV0LgyWvLbhjICfBzihppnTBGaOAPVaQhuWdqjyMYGLt_RwqjOKki4np_F-D9HSf3n5zv5OS_aBrB38YZPIpaeX6oiqWxxWVxldnleXC8yMyaoMosQPvsaSy6Di-mJq8xwCcE7B6pM_QJQ7zCA9tYca_-ad5nP1YzkyugZxU75GqgZkd8973zoZS1hSA47mGCwtaPalsvcdFo1fMfPIHgmp1seEh2r7j7URHav_TxT5YejvUqny6R559HFc_E8a41eDUQ1pr6smAyy2HWrAERuAz9D77T-Dl9W_XFxw4RGIYTQ-BEk5Qjvz6TVNDBNWogqI7ynwahgNg9V_TCVd9UwI44iIDo19kPlkaT8TJTNXlNcCqeUvbgCpzjfKuuoEqBxJu62ms8aaErmpLg7jx7gZye4CyA70xHYJkk9WK3OVdOe_2BlPUQCOQZT4zOaL0plfaL5KtE20o96n6uBfZbxQAJi59gJvY3x_cUP0g6K--NG7gyt60FP-GXRRJWjdJDAUgiU9idtawwI4hPUWC7gPjvRoTatzfSP97OVt6e2XQSOdwD6hBL2c5BqwnQFmKd4OTU9RO60fUxpqPDcvKgCNgFcG_tnDgojHm4DYBhuHrl0B4KNGuVpMxC3umP7jEw7VPW1cLGmKvBF4QAhJUsaqqRVzfo75zSUE1vTHr-DAc8QR9lvYXq71br3UuL2Xv5CGwguOIJXD-5jNtjUTiptrQhin4DhWzX6LA0wCbs0VgMUunnv0ntJaR399fo-bqeE2pzSwJfSX3n2L5nXFp-f-o4a_bzeWfInfbss_538QbNY-B7RgMT39KOAX2158jHdMjAju7uYitxLp50eUezZThpMdClw5s7pddt6PgCatHcg8TwouKB2d0yfefsyuKJm41-qhKR3dMpMZcLLt99oGQx-aqicv57SZokIMrwm5gy_cQY1yKrCYklkmZgH9zI5jS0qFsE9-eVN9liOBfaFTChR79GbATnX6tVJ1jOeWEnA2kFkbJt8DMKg8z21iQRqsq6m88P3FJJjdvjWmMZPOmaps3bGCAyw1NervqK4s1EAjjMtzecrpWxu8KNt5e9VRCPWX839V1JgS8mkrVSd8Y9U6b0yRZU08j4t6eza2RKKz1W5k2PiNRXlBUDEJHXrx0weTRTfySkSB4YiMrRTRufBLkdoPX8rDVP7GFmijlom2pClnuXud5HnPpQnHG3-BgXv94EOdCg4fYGXgJMc31FzKJFAkrp89vJDu4SwhhkZ1_1AkuK19Z4AdN0IViJf6ymJ1egz_NE6pLg-P_iiIZsU2T0hECKD_A.pjdiKh2sKWwlx4U3BAqcPnUvd_OUBad7uLDPoKYhHC6AMkr9LEcI9jlmgW9psb8kqlvd7rzZZPYdcOsImRZn3GV_WApwWIE34wmp4jiK4_QZXFzD6kvXD69Ezlij6Zohr1sMpCiN9a5-2S4qls2VGU8p7qVp4-J9C2nEL_IIRsyOlW09CgE89L4qnED9xa-BQ3CEybQL_2J2ZULtk3z_qlaqwgWJShNHt1CM8HUPKH0mmmjdsyZF1XFqUCDNmN0IkzdySeN-Dvrz-mtzKFSjSSkviIgnWdH38exwOG8tF84_H-M9x3m-AE5uv305Ojp46--dMPPM0ySiX6-i4vcsWQ', NULL, NULL, N'0982769343', N'0982769343', NULL, NULL, NULL, CAST(N'2021-01-13 13:40:42.647' AS DateTime), NULL, NULL, N'424c41dfe29011ea8a8d0a580a820a95', CAST(N'2021-01-13 13:42:28.427' AS DateTime), NULL, N'092063bf-e901-4855-aecf-28c5d016077c', NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'674468', N'dat3', NULL, NULL, NULL, NULL, NULL, N'dat3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'69984', N'yyyy', N'yyy', N'y', NULL, NULL, NULL, N'yyyy', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'123456')
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'71138', NULL, NULL, NULL, NULL, NULL, NULL, N'azz', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'714722', N'dd', NULL, NULL, NULL, NULL, NULL, N'ddđ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'801847', NULL, NULL, NULL, NULL, NULL, NULL, N'ty', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'812565', N'ea', N'ea', NULL, NULL, NULL, NULL, N'ea', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'815735', NULL, NULL, NULL, NULL, NULL, NULL, N'dat2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'98285', NULL, NULL, NULL, NULL, NULL, NULL, N'a', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'a78af722-366c-4443-ac27-311250fd633c', N'Mr.', N'thinhnc', N'thinhnc@htc.com.vn', NULL, NULL, NULL, N'Thinh', NULL, 0, NULL, NULL, NULL, NULL, N'098234532', NULL, NULL, NULL, NULL, CAST(N'2020-12-30 11:11:40.820' AS DateTime), NULL, CAST(N'2020-12-30 11:11:40.820' AS DateTime), N'cdadd8aa-b72d-416a-912f-b59a2735c165', CAST(N'2021-01-13 13:57:08.767' AS DateTime), NULL, N'a12833bd-7195-48fa-a7b1-9acb0bf67e87', NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'cdadd8aa-b72d-416a-912f-b59a2735c165', N'Mr.', N'pmkttest', N'pmkttest@htc.com.vn', NULL, NULL, NULL, N'User Test', NULL, 1, NULL, NULL, NULL, NULL, N'091398134', NULL, NULL, NULL, NULL, CAST(N'2021-02-24 22:41:16.110' AS DateTime), NULL, CAST(N'2020-12-09 16:12:03.167' AS DateTime), N'cdadd8aa-b72d-416a-912f-b59a2735c165', CAST(N'2021-01-13 13:56:41.507' AS DateTime), NULL, N'30dbd124-ee56-4a4b-bb75-827c5eb19ffe', NULL)
INSERT [dbo].[User] ([Id], [Title], [UserName], [Email], [FirstName], [MidName], [LastName], [FullName], [GroupId], [Status], [Note], [RefreshToken], [Groups], [ReceiveEmail], [Mobile], [Tel], [OtherEmail], [Avatar], [Language], [LastLoginAt], [CreatedBy], [CreatedAt], [ModifiedBy], [ModifiedAt], [IsAdmin], [GroupBireportId], [PassWord]) VALUES (N'dd127d08daa94b1fb9a6031b1f482f8e', N'Mr.', N'tuyendq3', N'tuyendq3@fpt.com.vn', N'Tuy?n', N'Quang', N'Ðàm', N'Ðàm Quang Tuy?n (FIS ERP HN)', NULL, 0, N'Ghi chu', N'bcKLVvFp4JN6SpPw5GKarURVXaBwgFQQBbrePVpUzRcAAQAAC0_QPwzrha1M6d6wp7bB3vJgI5cuPWdfQ8w3wtVAFDFMPxUmORXQHhf6ULJVp6d8G6e_NP9tsYPqneB5p3327BFzQcNlkCotbku7xPnNyxg91qH50a16iqPaL7g_GoJPZQDjK6xbrTSHOd2qsXbD9LwAQFUXpk9x7CSTbuzy4gBRlDmVWIv9dlTF9eY9duTuSaN8EsoEtkkdLZEjXTuQlT_UoK5lpdGYSN8DHZKIMjWUbOnkkH0Wz_XOjpRXQBK4wO-E6T2fGKv92In0CuoRoCmo8X_x0PWByAhE-qnbi0KY1-IRMmzpdvCsEJ-kZJpJon9dJVvLZWx2pcJ0fWkbBMAFAACc_kSlCht5aXdrNQFdsjKFVm19TMKv8DKPhDbfHxx7XEZTmx7bSk_zYvrk-b0oSzSvCQPPqECqOaFZKtgn6__uORuE2eThLmrqgmbmLXCIuzMsjBwq6J4BJzro5LGvatBivrzKRiEZtkAFycVHuelSsEIVTywnBy4_AETExr1Z8JCxo-yg7NDdLmPOx4YPnnw0QF1hpvSfWLGBDbAHMCcnIcaJsaRiGGd4Lg5RNGz1QFuHWiL5lEi4E6dVI7PA6CJ3rM-n_yr4lKT--AxDAAgnpdU18WFNJme3thNe1FLI3JzoezDJPD2YVoqcWHwKbfsu3tJtEpWpycuux6-1nlwAl6Zk16RYKYBakboQHiLNiEQh3HU_OEwYppJHV9yYEy4qiIXwFGfPS5p21B25mxJfUu36W6TAPn4mHSM_5p8zUcdCsGx3nWRbutJtIUvjrv3_Thu9AI1CvZAj0H8VAlVsNU7LRr_euCF5uBGhQtSNnbhyydIPguTi1ZifuWizGZJy3ihMWU5ggd6eNj26kZYnTr0SVPUwC-7yjjvdkvxOVIgOlIovtgv3ztAvyml4EQ3WKPQ081g2-6iED2pzlVkKwL8I4rqXRmn6tVg-Ye8o92hGEFZaZChxjjwUO_m-CSBw3rwNs9ydinQUlPdUK3AffLIYcaoWsCpm_vFXh30S0niZCOnXXpr-JKCvtG9KHs_j5MJdJ5g-dIUT2h6JPXjRa1nEeJHyFCgEh9xVuwcCDryED9ukdP150QJnL4PJX2s_r-QXBt8JgX8GZ-1J20xXKIGVJUJPbwi3mJtTkZhtXCrmea2omPLFO_XNIRwQop9pEcvlvRovcIGeCWryJJPRaqZT_Y0KDYmFBxP5mJbynKhX6Y-f46-Pe0zuF4qwbzO0Nv2FooDJ9f4_eYeYt-aJMnXyGpF3z3ZKPjmfF1re2TZJ-4Zhtuq5a3RW5dkhTZabjK_vV8QmN-Db3VRDY3a66scWWOxbVK8FLazmTGtDzLmCMuUQ1YQ-bU_RGlX3vqW5cGJ9o5HipLVUEnasULe7ZuKOyQGB84Z9Z6HtWNk0bS6TN8v7rxSiv95uVst1EOKRLu-yxfvAwrzl_tVcjwuRQDieAUEaiA4wjuC8wXotT0mLt6ir0NjKwVPwrwp9RH27ZxWjx90uBczXukXxHEWFgGLuZXVi3rOG_Y1RmGRb4Eun0diU8lA_s68_dmvgnxZhdNjf81OjsKkdRNyV0VWwgRfBWFnMKUi-QAGaW_fG-oOOIVfxnVjWbroh4on1rCw0tLS2ZiKs2WfKyTpjg_YhdxZRU6pppTYCCuFAjJ0u_-snF1Asci_zIVh2BhX9jd_tu9D8e1PnOQSX-mNcBOkC0EqikoOUMCad7n3PifRvMEwLEscWccCKGcGLcN71TUTGdOSnIy4KClEBrcNN8dHQl-RrpSY9RrYwMdanL9acuBMd5Uiv6KOQpD7ap0mSeZVxlA95kqKujqjGzQx5d2gaQx5PtFoo7TgyCSwuuA-EtSY9xw67-sHGH3xsEZo6m1b_0VD_f2DnZpdn9EtV570GakiXwtmcWWfcEnNVAVsT4ZQ28I9d8MVQwCfK2bBnAuAkCooxSj6oszuAwQG1mUbZbX6bvvqlabFUkzN-ZomsWNcaqQhKKF15tALfkHWo0UAWdsdWReH-z0j7U4leMekb4f80DBxGjsxv_JDMIk8CP-Tcyzf2HUvCONLKnraooJZPEKqYGvqsaVuHu6Tq3bZjaUrDhYyYR97xw1wiPxIIDAChC7MoKh36Ks0tXhMGSFdkq1tyYghU97Ye0rhx1Io4UmdlXBoupET2WZMIeGzERfWwg3HqKW6C_yFGljtN_71hY_Kl6nTg2QNNXHvAZZqmRsP1-nXXmKO9xpCBJZbcKkz18ydzd9Jq9y0yKo2JW0zSe6U8YX0OWv1kkKxl_8v_G2fWETIA3zvvzpaXvAowyQ.hdH3yrqX7i5yFT4I0Qu0dK_qEP_XGAmG_c85Xi9LBwoMsYHVotODqlVJUyB26EvMY9fdtEnZd8ToppQR5MOu648Vom9YXHH6MamkrUctl4GLSlJVw7frmO9uYrTwimFGPzeI5NQWg2QycRv7L77VepaJdVYx8c5m0D-E2JyLubNjeW2GQQA3U1DOGK4KW_O_sXJtNOudnVHPPPDxBGvQ_S_4OoWDBeWBrDAxm_qpuYyL0I2JgQ-lkB47XZWEKMFx9xau-tL2u9c3yzi4Tvo2f02lVYuPvRTPLNmWEfYsWYrFESZxdx95_n3-BMlw5dkCjnOXo83vVF9vL0m7h8C8IA', NULL, NULL, N'0976912503', N'0976912503', NULL, NULL, NULL, CAST(N'2020-11-03 10:00:03.000' AS DateTime), N'dd127d08daa94b1fb9a6031b1f482f8e', CAST(N'2020-10-01 14:48:54.000' AS DateTime), N'424c41dfe29011ea8a8d0a580a820a95', CAST(N'2021-01-13 13:28:23.383' AS DateTime), NULL, N'30dbd124-ee56-4a4b-bb75-827c5eb19ffe', NULL)
INSERT [dbo].[UserBussinessGroup] ([Id], [UserId], [BussinessGroupId]) VALUES (N'05d870fa-3dbc-4d6f-a491-0ccd87d95269', N'14734382802c4e5b85af56bdf7cc823a', N'b18f7149f7df4c86b72e64c584e8fcc4')
INSERT [dbo].[UserBussinessGroup] ([Id], [UserId], [BussinessGroupId]) VALUES (N'10fa7d24-e578-420b-a4f0-3aed40bd47b5', N'31bb2f3b-b900-470a-823d-ae644768877d', N'6158f9bf865f42dba520cf81bce0f383')
INSERT [dbo].[UserBussinessGroup] ([Id], [UserId], [BussinessGroupId]) VALUES (N'353d4716-bf99-4897-ba31-4a3589883446', N'a78af722-366c-4443-ac27-311250fd633c', N'fad80121be254063acdde6cbd560c587')
INSERT [dbo].[UserBussinessGroup] ([Id], [UserId], [BussinessGroupId]) VALUES (N'7a88b317-750a-4357-9d1d-09ccab66b386', N'cdadd8aa-b72d-416a-912f-b59a2735c165', N'92dca439071e434db11452b3cefddc57')
INSERT [dbo].[UserBussinessGroup] ([Id], [UserId], [BussinessGroupId]) VALUES (N'7ba416df-ce61-4f84-9f81-2fb8fb715e8a', N'cdadd8aa-b72d-416a-912f-b59a2735c165', N'6158f9bf865f42dba520cf81bce0f383')
INSERT [dbo].[UserBussinessGroup] ([Id], [UserId], [BussinessGroupId]) VALUES (N'8fcc25e3-64fb-496f-bf5c-b7ff73a82508', N'cdadd8aa-b72d-416a-912f-b59a2735c165', N'd449079ebda449829dfe9887da2fc292')
INSERT [dbo].[UserBussinessGroup] ([Id], [UserId], [BussinessGroupId]) VALUES (N'933ba396-be81-45fd-83d7-21c077c01ff3', N'14734382802c4e5b85af56bdf7cc823a', N'd449079ebda449829dfe9887da2fc292')
INSERT [dbo].[UserBussinessGroup] ([Id], [UserId], [BussinessGroupId]) VALUES (N'b3731542-d7fd-4a1a-9be4-44920b961b80', N'654d1d7610b74398ab7c0edc506cb58f', N'92dca439071e434db11452b3cefddc57')
INSERT [dbo].[UserBussinessGroup] ([Id], [UserId], [BussinessGroupId]) VALUES (N'b7e7419f-8577-4ed9-9c55-dc9d24bb80ee', N'654d1d7610b74398ab7c0edc506cb58f', N'7968f824d8624313abe2a7b13b127431')
INSERT [dbo].[UserBussinessGroup] ([Id], [UserId], [BussinessGroupId]) VALUES (N'd385b9c5-7482-49fb-99b5-393a661f9814', N'a78af722-366c-4443-ac27-311250fd633c', N'7968f824d8624313abe2a7b13b127431')
INSERT [dbo].[UserBussinessGroup] ([Id], [UserId], [BussinessGroupId]) VALUES (N'dd2c3076-91ac-47cc-b6b8-bd1e67ff6f72', N'424c41dfe29011ea8a8d0a580a820a95', N'6158f9bf865f42dba520cf81bce0f383')
INSERT [dbo].[UserGroupUser] ([Id], [UserId], [GroupUserId]) VALUES (N'1805e64b-c6f7-4d01-96b1-ac3b0ae8cc10', N'cdadd8aa-b72d-416a-912f-b59a2735c165', N'1ea8cdde55034d85860218e32a76fbc7')
INSERT [dbo].[UserGroupUser] ([Id], [UserId], [GroupUserId]) VALUES (N'42ee4896-137c-4701-a016-a17aad7098e5', N'424c41dfe29011ea8a8d0a580a820a95', N'082832ac-ae3c-4908-8cc4-c5e6df4773c7')
INSERT [dbo].[UserGroupUser] ([Id], [UserId], [GroupUserId]) VALUES (N'5dad0c1b-2ab8-41e2-adf2-63fcb34cee4c', N'31bb2f3b-b900-470a-823d-ae644768877d', N'082832ac-ae3c-4908-8cc4-c5e6df4773c7')
INSERT [dbo].[UserGroupUser] ([Id], [UserId], [GroupUserId]) VALUES (N'7aace8e6-9e21-4b6b-ab47-69601b37150a', N'14734382802c4e5b85af56bdf7cc823a', N'082832ac-ae3c-4908-8cc4-c5e6df4773c7')
INSERT [dbo].[UserGroupUser] ([Id], [UserId], [GroupUserId]) VALUES (N'7b88f4e4-3a70-47ce-9ad5-2af7ee415ff7', N'dd127d08daa94b1fb9a6031b1f482f8e', N'1ea8cdde55034d85860218e32a76fbc7')
INSERT [dbo].[UserGroupUser] ([Id], [UserId], [GroupUserId]) VALUES (N'abacdc84-15eb-41ee-8785-4da59da98578', N'14734382802c4e5b85af56bdf7cc823a', N'1ea8cdde55034d85860218e32a76fbc7')
INSERT [dbo].[UserGroupUser] ([Id], [UserId], [GroupUserId]) VALUES (N'e88706ea-01be-45fe-8441-aab817e56c6f', N'654d1d7610b74398ab7c0edc506cb58f', N'082832ac-ae3c-4908-8cc4-c5e6df4773c7')
INSERT [dbo].[UserGroupUser] ([Id], [UserId], [GroupUserId]) VALUES (N'ff67b498-4414-40f9-a0eb-28e3339b1ffb', N'a78af722-366c-4443-ac27-311250fd633c', N'082832ac-ae3c-4908-8cc4-c5e6df4773c7')
INSERT [dbo].[UserRole] ([Id], [UserId], [RoleId]) VALUES (N'1', N'10764', N'5b86edcb09ec4453895bf55620605214')
INSERT [dbo].[UserRole] ([Id], [UserId], [RoleId]) VALUES (N'2', N'69984', N'6393d4f7330141fba8a5646868abe49c')
ALTER TABLE [dbo].[F_CSKH.05]  WITH CHECK ADD  CONSTRAINT [FK_F_CSKH.05_User] FOREIGN KEY([idUser])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[F_CSKH.05] CHECK CONSTRAINT [FK_F_CSKH.05_User]
GO
ALTER TABLE [dbo].[F_CSKH05_TRASH]  WITH CHECK ADD  CONSTRAINT [FK_F_CSKH05_TRASH_User] FOREIGN KEY([idUser])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[F_CSKH05_TRASH] CHECK CONSTRAINT [FK_F_CSKH05_TRASH_User]
GO
ALTER TABLE [dbo].[RoleRoleDetail]  WITH CHECK ADD  CONSTRAINT [FK_RoleRoleDetail_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([Id])
GO
ALTER TABLE [dbo].[RoleRoleDetail] CHECK CONSTRAINT [FK_RoleRoleDetail_Role]
GO
ALTER TABLE [dbo].[RoleRoleDetail]  WITH CHECK ADD  CONSTRAINT [FK_RoleRoleDetail_RoleDetail] FOREIGN KEY([RoleDetailId])
REFERENCES [dbo].[RoleDetail] ([Id])
GO
ALTER TABLE [dbo].[RoleRoleDetail] CHECK CONSTRAINT [FK_RoleRoleDetail_RoleDetail]
GO
ALTER TABLE [dbo].[UserBussinessGroup]  WITH CHECK ADD  CONSTRAINT [FK_UserBussinessGroup_BussinessGroup] FOREIGN KEY([BussinessGroupId])
REFERENCES [dbo].[BussinessGroup] ([Id])
GO
ALTER TABLE [dbo].[UserBussinessGroup] CHECK CONSTRAINT [FK_UserBussinessGroup_BussinessGroup]
GO
ALTER TABLE [dbo].[UserBussinessGroup]  WITH CHECK ADD  CONSTRAINT [FK_UserBussinessGroup_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[UserBussinessGroup] CHECK CONSTRAINT [FK_UserBussinessGroup_User]
GO
ALTER TABLE [dbo].[UserGroupUser]  WITH CHECK ADD  CONSTRAINT [FK_UserGroupUser_GroupUser] FOREIGN KEY([GroupUserId])
REFERENCES [dbo].[GroupUser] ([Id])
GO
ALTER TABLE [dbo].[UserGroupUser] CHECK CONSTRAINT [FK_UserGroupUser_GroupUser]
GO
ALTER TABLE [dbo].[UserGroupUser]  WITH CHECK ADD  CONSTRAINT [FK_UserGroupUser_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[UserGroupUser] CHECK CONSTRAINT [FK_UserGroupUser_User]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([Id])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_Role]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_User]
GO
