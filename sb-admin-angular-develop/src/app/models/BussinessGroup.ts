import { UserBussinessGroup } from "./UserBussinessGroup"

export class BussinessGroup {
    Id?: string
    Code?: string
    Name?: string
    Status?: boolean
    Note?: string
    CreatedBy?: string
    CreatedAt?: Date
    ModifiedBy?: string
    ModifiedAt?: Date
    UserBussinessGroup: UserBussinessGroup[] = []
}