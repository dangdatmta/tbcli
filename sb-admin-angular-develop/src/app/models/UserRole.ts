import { User } from "@testing/mocks"
import { Role } from "./Role"

export class UserRole {

    constructor(roleId: string){
        this.RoleId = roleId;
    }

    Id?: string
    UserId?: string
    RoleId?: string
    User?: User
    Role?: Role
}