export class Region {
    Id?: string;
    AreaId?: string;
    Code?: string;
    Name?: string;
    RegionLevel?: number;
    RegionPath?: string;
    ParentId?: string;
    BiCode?: string;
    CreatedBy?: string;
    CreatedAt?: Date;
    ModifiedBy?: string;
    ModifiedAt?: Date;
    Status?: boolean;

}