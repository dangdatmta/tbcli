import { Role } from "./Role";
import { UserBussinessGroup } from "./UserBussinessGroup";
import { UserRole } from './UserRole';


export class User {
    Id?:string;
    Title?:string;
    UserName?:string;
    Email?:string;
    FirstName?:string;
    MidName?:string;
    LastName?:string;
    FullName?:string;
    GroupId?:Int16Array;
    Status?:boolean;
    Note?:string;
    RefreshToken?:string;
    Groups?:string;
    ReceiveEmail?:string;
    Mobile?:string;
    Tel?:string;
    OtherEmail?:string;
    Avatar?:string;
    Language?:string;
    LastLoginAt?:Date;
    CreatedBy?:string;
    CreatedAt?:Date;
    ModifiedBy?:string;
    ModifiedAt?:Date;
    IsAdmin?:boolean;
    GroupBireportId?:string;
    PassWord?:string;
    UserRoles?:UserRole[]
    //Roles?:Role[];
    // UserBussinessGroup: UserBussinessGroup[] = []
}