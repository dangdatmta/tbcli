export class Role {
    Id?:string
    Code?:string
    Name?:string
    IsActive?:boolean
    CreatedBy?:string
    CreatedAt?:Date
    ModifiedBy?:string
    ModifiedAt?:Date

}