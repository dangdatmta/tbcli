// import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { SearchFilterPipe } from './search-filter.pipe';

@NgModule({
    declarations: [AppComponent, SearchFilterPipe],
    // imports: [BrowserModule, AppRoutingModule, HttpClientModule, Ng2OrderModule],
    imports: [ AppRoutingModule,BrowserModule,BrowserAnimationsModule,HttpClientModule,CommonModule],

    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
