import { Pipe, PipeTransform } from '@angular/core';
import { User } from '@app/models/User';

@Pipe({
  name: 'searchFilter'
})
export class SearchFilterPipe implements PipeTransform {

  transform(users: User[], searchValue: string): User[] {
    if(!users || !searchValue){
      return users;
    }
    return users.filter(users=>
      users.FullName?.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase())||
      users.Title?.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase())||
      users.UserName?.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase())||
      users.Email?.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase())||
      users.Mobile?.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase())
      );
  }

}

