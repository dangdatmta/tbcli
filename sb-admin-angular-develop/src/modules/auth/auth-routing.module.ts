/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SBRouteData } from '@modules/navigation/models';

/* Module */
import { AuthModule } from './auth.module';

/* Containers */
import * as authContainers from './containers';

/* Guards */
import * as authGuards from './guards';

/* Routes */
export const ROUTES: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'login',
    },
    {
        path: 'login',
        canActivate: [],
        component: authContainers.LoginComponent,
        data: {
            title: 'Đăng nhập',
        } as SBRouteData,
    },
    {
        path: 'register',
        canActivate: [],
        component: authContainers.RegisterComponent,
        data: {
            title: 'Pages Register - SB Admin Angular',
        } as SBRouteData,
    },
    {
        path: 'forgot-password',
        canActivate: [],
        component: authContainers.ForgotPasswordComponent,
        data: {
            title: 'Pages Forgot Password - SB Admin Angular',
        } as SBRouteData,
    },
    {
        path: 'region',
        canActivate: [],
        component: authContainers.RegionComponent,
        data: {
            title: 'Vùng miền',
        } as SBRouteData,
    },
    {
        path: 'quan-tri-nguoi-dung',
        canActivate: [],
        component: authContainers.QuanTriNguoiDungComponent,
        data: {
            title: 'Thêm và phân quyền người dùng',
        } as SBRouteData,
    },
    {
        path: 'quan-tri-nhom-quyen',
        canActivate: [],
        component: authContainers.QuanTriNhomQuyenComponent,
        data: {
            title: 'Quản trị nhóm quyền',
        } as SBRouteData,
    },
    // {
    //     path: 'quan-tri-nhom-quyen',
    //     canActivate: [],
    //     component: authContainers.QuanTriNhomQuyenComponent,
    //     data: {
    //         title: 'Thêm và phân quyền người dùng',
    //     } as SBRouteData,
    // },
    {
        path: 'quan-tri-phan-quyen',
        canActivate: [],
        component: authContainers.QuanTriPhanQuyenComponent,
        data: {
            title: 'Thêm và phân quyền người dùng',
        } as SBRouteData,
    },
    {
        path: 'Embed',
        canActivate: [],
        component: authContainers.EmbeddedIframeComponent,
        data: {
            title: 'Embed',
        } as SBRouteData,
    },
    {
        path: 'CSKH',
        canActivate: [],
        component: authContainers.StaticComponent,
        data: {
            title: 'Chăm sóc khách hàng',
        } as SBRouteData,
    },
];

@NgModule({
    imports: [AuthModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class AuthRoutingModule {}
