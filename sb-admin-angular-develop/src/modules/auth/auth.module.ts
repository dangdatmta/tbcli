/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

/* Modules */
import { AppCommonModule } from '@common/app-common.module';
import { NavigationModule } from '@modules/navigation/navigation.module';

/* Components */
import * as authComponents from './components';

/* Containers */
import * as authContainers from './containers';

/* Guards */
import * as authGuards from './guards';

/* Services */
import * as authServices from './services';
// <<<<<<< HEAD
import { QuanTriNguoiDungComponent } from './containers/quan-tri-nguoi-dung/quan-tri-nguoi-dung.component';
import { QuanTriNhomQuyenComponent } from './containers/quan-tri-nhom-quyen/quan-tri-nhom-quyen.component';
import { QuanTriPhanQuyenComponent } from './containers/quan-tri-phan-quyen/quan-tri-phan-quyen.component';
// =======
//import { QuanTriNguoiDungComponent } from './containers/quan-tri-nguoi-dung/quan-tri-nguoi-dung.component';

import { SearchFilterPipe } from './containers/quan-tri-nguoi-dung/search-filter.pipe';
// import { QuanTriNhomQuyenComponent } from './containers/quan-tri-nhom-quyen/quan-tri-nhom-quyen/quan-tri-nhom-quyen.component';
// >>>>>>> 3b594c34ac70d4aed2c093742680ed1dbcd7d756

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        AppCommonModule,
        NavigationModule,
        
    ],
    providers: [...authServices.services, ...authGuards.guards],
// <<<<<<< HEAD
    declarations: [...authContainers.containers, ...authComponents.components, QuanTriNguoiDungComponent, QuanTriNhomQuyenComponent, QuanTriPhanQuyenComponent],
// =======
//     declarations: [...authContainers.containers, ...authComponents.components, SearchFilterPipe],
//  >>>>>>> 3b594c34ac70d4aed2c093742680ed1dbcd7d756
    exports: [...authContainers.containers, ...authComponents.components],
})
export class AuthModule {}
