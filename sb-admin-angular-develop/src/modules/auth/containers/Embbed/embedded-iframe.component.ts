import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
declare var tableau: any;
@Component({
  selector: 'sb-embedded-iframe',
  templateUrl: './embedded-iframe.component.html',
  styleUrls: ['./embedded-iframe.component.scss']
})
export class EmbeddedIframeComponent implements AfterViewInit {

  constructor() { }
  viz: any;
  @ViewChild("vizContainer") containerDiv?: ElementRef;

  ngAfterViewInit() {
    this.initTableau();
  }

  initTableau() {
    // const containerDiv = document.getElementById("vizContainer");
    const vizUrl =
      "https://public.tableau.com/views/COVID-19DataHubGlobalTracker/GLOBAL?:language=en&:embed=y&;publish=yes&:origin=viz_share_link&:toolbar=n&:display_count=y";
    const options = {
      hideTabs: true,
      onFirstInteractive: () => {
        console.log("onFirstInteractive");
      },
      onFirstVizSizeKnown: () => {
        console.log("onFirstVizSizeKnown");
      }
    };
    this.viz = new tableau.Viz(
      this.containerDiv?.nativeElement,
      vizUrl,
      options
    );
  }

}
