import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbeddedIframeComponent } from './embedded-iframe.component';

describe('EmbeddedIframeComponent', () => {
  let component: EmbeddedIframeComponent;
  let fixture: ComponentFixture<EmbeddedIframeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmbeddedIframeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbeddedIframeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
