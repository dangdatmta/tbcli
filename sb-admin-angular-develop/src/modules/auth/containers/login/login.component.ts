import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@modules/auth/services/auth.service';
import { NgForm } from '@angular/forms';
import { User } from '@app/models/User';
import { MessageService } from 'primeng/api';
import { DashboardService } from '@modules/dashboard/services';

@Component({
    selector: 'sb-login',
    templateUrl: './login.component.html',
    styleUrls: ['login.component.scss'],
    providers: [DashboardService]
})
export class LoginComponent implements OnInit {
    countUserRole?: number
    countRoleRoleDetails?: number
    FullName?: string
    roleDetail: string[] = []
    role: string[] = []
    isLoginError: boolean = false;
    loginform?: User
    constructor(private serverHttp: AuthService,
                private router: Router,
                private messageService: MessageService) { }
    ngOnInit() {
        this.resetForm()
    }
    resetForm(form?: NgForm) {
        if (form != null)
            form.reset();
        this.loginform = {
            UserName: '',
            PassWord: ''
        }
    }
    OnSubmit(loginForm: NgForm) {
        this.serverHttp.userAuthentication(loginForm.value).subscribe((data: any) => {
            //this.userName = loginForm.value.FullName
            //localStorage.setItem('userToken', data.access_token);
            if (data == "Login failed!") {
                this.messageService.add({ key: 'br', severity: 'error', summary: 'Thất bại', detail: 'Tài khoản hoặc mật khẩu không đúng', life: 3000 });
                this.isLoginError = true;
            }
            else {
                this.countUserRole = data.Profile.countUserRole
                this.countRoleRoleDetails = data.Profile.countRoleRoleDetails
                console.log('x', this.countUserRole)
                console.log('x', this.countRoleRoleDetails)
                this.FullName = data.Profile.FullName

                for (let i = 0; i < data.Profile.countUserRole; i++) {
                    this.role[i] = data.Profile.RoleDetails.UserRoles[i].Role.Name
                    for (let j = 0; j < data.Profile.countRoleRoleDetails; j++) {
                        this.roleDetail[j] = data.Profile.RoleDetails.UserRoles[i].Role.RoleRoleDetails[j].RoleDetail.Name
                    }
                }
                console.log('roleName', this.role)
                console.log('roledetailName', this.roleDetail)

                // this.roleDetail = data.Profile.RoleDetails.UserRoles[0].Role.RoleRoleDetails[0].RoleDetail.Name
                //set giá trị cho quản trị module
                this.serverHttp.setUserName(this.FullName)
                this.serverHttp.setRoleDetail(this.roleDetail)
                this.serverHttp.setRoleName(this.role)
                this.router.navigate(['/auth/Embed']);
            }
        },
            (err: HttpErrorResponse) => {
                this.isLoginError = true;
            });
    }
}
