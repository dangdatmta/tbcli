import { Component, NgModule, OnInit } from '@angular/core';
import { Role } from '@app/models/Role';
import { User } from '@app/models/User';
import { UserRole } from '@app/models/UserRole';
import { AuthService } from '@modules/auth/services';
import { MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'sb-quan-tri-nguoi-dung',
  templateUrl: './quan-tri-nguoi-dung.component.html',
  styleUrls: ['./quan-tri-nguoi-dung.component.scss']
})
export class QuanTriNguoiDungComponent implements OnInit {
  roleDetail: string[] = []
  roleName: string[] = []
  FullName?: string
  roles: UserRole[] = []
  titleDialog?: boolean
  roleSelected?: number
  User: User[] = []
  Role: Role[] = []
  role?: Role
  userRoles: UserRole[] = []
  user?: User
  EditDialog?: boolean
  productDialog?: boolean
  submitted?: boolean

  constructor(private serverHttp: AuthService,
    private messageService: MessageService,
    private confirmService: ConfirmationService) { }
  ngOnInit(): void {
    this.FullName = this.serverHttp.getUserName()
    this.roleDetail = this.serverHttp.getRoleDetail()
    this.roleName = this.serverHttp.getRoleName()
    console.log('length',this.roleName.length)
    this.getAllUser()
    this.getAllRole()
    this.roleSelected = this.Role.length
    this.roles = new Array<UserRole>();
  }
  getAllUser() {
    this.serverHttp.getUsers().subscribe((data) => {
      console.log('data', data);
      this.User = data;
    });
  }
  getAllRole() {
    this.serverHttp.getRole().subscribe((data) => {
      console.log('role', data);
      this.Role = data;
    });
  }

  openNew() {
    this.user = {};
    this.submitted = false;
    this.EditDialog = true;
  }
  edit(user: User) {
    this.user = { ...user };
    this.EditDialog = true;
  }
  hideDialog() {
    this.EditDialog = false;
    this.submitted = false;
  }

  save() {
    this.submitted = true;
    debugger
    this.user = { ...this.user, UserRoles: this.userRoles };
    this.messageService.add({ key: 'br', severity: 'success', summary: 'Thành công', detail: 'Tạo nhân viên thành công', life: 3000 });
    // console.log('x', this.user)
    // if (this.user?.Id) {
    //   this.User[this.findIndexById(this.user.Id)] = this.user;
    //   //this.role.IsActive = this.roleSelected
    //   // this.serverHttp.updateUser(this.role.Id, this.role).subscribe((data) => { console.log('data', data) }
    //   // );
    //   // this.getAllRole();
    //   // this.messageService.add({ key: 'br', severity: 'success', summary: 'Thành công', detail: 'Cập nhật thành công', life: 3000 });
    // }
    // else {
    //this.product.id = this.createId();

    this.serverHttp.addUser(this.user as User).subscribe((data) => { console.log('Users', data) });
    this.getAllUser()

    // }
    // this.User = [...this.User];
    this.user = {}
    this.EditDialog = false;
  }


  findIndexById(id: string): number {
    let index = -1;
    for (let i = 0; i < this.User.length; i++) {
      if (this.User[i].Id === id) {
        index = i;
        break;
      }
    }
    return index;
  }
  getRoleId(e: any, id: string) {
    if (e.target.checked) {
      this.userRoles.push(new UserRole(id));
    } else {
      console.log(id + 'unchecked')
      this.userRoles = this.userRoles.filter(m => m.RoleId != id)
    }
    console.log(this.userRoles)
  }

  delete(user: User) {
    this.confirmService.confirm({
      message: 'Bạn có chắc muốn xóa ' + user.FullName + '?',
      header: 'Xác nhận',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.serverHttp.deleteRole(user.Id as string).subscribe((data) => { console.log('data', data) }
        );
        // this.role = {};
        // debugger
        // this.getAllRole();
        this.User = this.User.filter(val => val.Id !== user.Id);
        this.user = {};
        this.messageService.add({ key: "br", severity: 'success', summary: 'Thành công', detail: 'Xóa thành công', life: 3000 });
      }
    });
  }
}
