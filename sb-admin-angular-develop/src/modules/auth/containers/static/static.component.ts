import { ChangeDetectionStrategy, Component, OnInit, ViewChild } from '@angular/core';
import { F_CSKH05 } from '@app/models/F_CSKH05';
import { AuthService } from '@modules/auth/services';

@Component({
    selector: 'sb-static',

    templateUrl: './static.component.html',
    styleUrls: ['static.component.scss'],
})
export class StaticComponent implements OnInit {
    @ViewChild('fileInput') fileInput
    versions: number[] = []
    public isCollapsed = false
    message?: string;
    timeUpdate?: Date;
    userCreate?: string;
    F_CSKH05S: F_CSKH05[] = []
    f_cskh05?: F_CSKH05
    countRecords: number[] = []

    constructor(
        private serverHttp: AuthService
    ) { }
    ngOnInit(): void {
        this.getAllVersion();
    }
    
    getAllVersion() {
        this.serverHttp.getListVersion().subscribe((data) => {
            console.log('version', data);
            this.versions = data;
            for (var i = 0; i < this.versions.length; i++) {
                this.serverHttp.getCSKH_05_ByVersion(this.versions[i]).subscribe((data) => {
                    this.F_CSKH05S.push(data)
                    this.countRecords[i] = data.length
                });
            }

        });
    }

    uploadFile() {
        let formData = new FormData();
        formData.append('upload', this.fileInput.nativeElement.files[0])

        this.serverHttp.UploadExcel(formData).subscribe(result => {
            this.message = result.toString();
            this.getAllVersion();
        });
    }
    title = 'ThachBanCli';
}
