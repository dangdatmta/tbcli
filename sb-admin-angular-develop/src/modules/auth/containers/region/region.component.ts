import { AfterViewInit, Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Region } from '@app/models/Region';
import { AuthService } from '@modules/auth/services';

@Component({
  selector: 'sb-region',
  templateUrl: './region.component.html',
  styleUrls: ['./region.component.scss'],
})
export class RegionComponent implements OnInit {
  searchValue?: string
  EditDialog?: boolean
  submitted?: boolean
  id?: string
  public Region: Region[] = []
  region?: Region
  statuses?: any[];
  constructor(
    public sanitizer: DomSanitizer,
    private serverHttp: AuthService
  ) { this.sanitizer = sanitizer; }

  ngOnInit(): void {
    this.loadAllRegion();
    this.statuses = [
      { label: 'Active', value: 'true' },
      { label: 'InActive', value: 'false' }
    ];
  }

  loadAllRegion() {
    this.serverHttp.getRegion().subscribe((data) => {
      console.log('data', data);
      this.Region = data;
    });
  }
  getRegionById(id) {
    this.EditDialog = true;
    this.serverHttp.getRegionById(id).subscribe((data) => {
      console.log('data', data);
    });
  }
  edit(region: Region) {
    this.region = { ...region };
    this.EditDialog = true;
  }

  // <<<<<<< HEAD
  // editProduct(product: Product) {
  //   this.product = {...product};
  //   this.productDialog = true;
  // }

  // hideDialog() {
  //   this.productDialog = false;
  //   this.submitted = false;
  // }
  // =======
  hideDialog() {
    this.EditDialog = false;
    this.submitted = false;
  }
  // >>>>>>> 3b594c34ac70d4aed2c093742680ed1dbcd7d756



}
