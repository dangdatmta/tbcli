import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginForm } from '@app/models/LoginForm';
import { Role } from '@app/models/Role';
import { User } from '@app/models/User';
import { Observable, of, Subject, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AuthService {
    userName?: string
    roleDetail: string[] = []
    role: string[] = []
    setRoleName(data){
        this.role = data
    }
    getRoleName(){
        return this.role
    }
    setRoleDetail(data){
        this.roleDetail = data
    }
    getRoleDetail(){
        return this.roleDetail
    }
    setUserName(data){
        this.userName = data
    }
    getUserName(){
        return this.userName
    }
    private httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            //Authorization: 'my-auth-token'
        })
    };

    private REST_API_SERVER = "https://localhost:44363/api"
    constructor(private httpClient: HttpClient) { }
    public getRegion(): Observable<any> {
        const url = `${this.REST_API_SERVER}/region`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    public getUsers(): Observable<any> {
        const url = `${this.REST_API_SERVER}/user`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    public getRole(): Observable<any> {
        const url = `${this.REST_API_SERVER}/role`;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public addRole(data: Role): Observable<any> {
        const url = `${this.REST_API_SERVER}/role`;
        return this.httpClient
            .post<any>(url, data, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public addUser(data: User): Observable<any> {
        const url = `${this.REST_API_SERVER}/user`;
        return this.httpClient
            .post<any>(url, data, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public updateRole(roleId: string, data: Role) {
        const url = `${this.REST_API_SERVER}/role/` + roleId;
        return this.httpClient
            .put<any>(url, data, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public deleteUser(userId: string) {
        const url = `${this.REST_API_SERVER}/user/` + userId;
        return this.httpClient
            .delete<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public deleteRole(roleId: string) {
        const url = `${this.REST_API_SERVER}/role/` + roleId;
        return this.httpClient
            .delete<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }
    public getRegionById(regionId: string) {
        const url = `${this.REST_API_SERVER}/region/` + regionId;
        return this.httpClient
            .get<any>(url, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    //login
    public userAuthentication(loginForm: User) {
        const url = `${this.REST_API_SERVER}/auth/login`;
        
        const body: User = {
            UserName: loginForm.UserName,
            PassWord: loginForm.PassWord
        }
        return this.httpClient
            .post<any>(url, body, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    //bao cao cham soc khach hang
    public getListVersion():Observable<any>{
        const url = `${this.REST_API_SERVER}/excel`;
        return this.httpClient
        .get<any>(url,this.httpOptions)
        .pipe(catchError(this.handleError));
      }
      public getCSKH_05():Observable<any>{
        const url = `${this.REST_API_SERVER}/excel`;
        return this.httpClient
        .get<any>(url,this.httpOptions)
        .pipe(catchError(this.handleError));
      }
      public getCSKH_05_ByVersion(version?:number):Observable<any>{
        const url = `${this.REST_API_SERVER}/excel/`+version;
        return this.httpClient
        .get<any>(url,this.httpOptions)
        .pipe(catchError(this.handleError));
      }
      public UploadExcel(formData: FormData) {
        const url = `${this.REST_API_SERVER}/excel`;
        let headers = new HttpHeaders();
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        const httpOptions = { headers: headers };
        return this.httpClient.post(url, formData, httpOptions)
      }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong.
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // Return an observable with a user-facing error message.
        return throwError(
            'Something bad happened; please try again later.');
    }
    getAuth$(): Observable<{}> {
        return of({});
    }
}
