import { SideNavItems, SideNavSection } from '@modules/navigation/models';

export const sideNavSections: SideNavSection[] = [
    // {
    //     text: 'CORE',
    //     items: ['dashboard'],
    // },
    {
        text: 'CHỨC NĂNG',
        items: ['layouts', 'pages','reports'],
    },
    // {
    //     text: 'ADDONS',
    //     items: ['charts', 'tables'],
    // },
];

export const sideNavItems: SideNavItems = {
    dashboard: {
        icon: 'tachometer-alt',
        text: 'Dashboard',
        link: '/dashboard',
    },
    layouts: {
        icon: '',
        text: 'NHẬP DỮ LIỆU ',
        submenu: [
            {
                text: 'Chăm sóc khách hàng',
                link: '/auth/CSKH',
            },
            // {
            //     text: 'Light Sidenav',
            //     link: '/dashboard/light',
            // },
        ],
    },
    pages: {
        icon: 'book-open',
        text: 'Quản trị',
        submenu: [
            {
                text: 'Master Data',
                submenu: [
                    {
                        text: 'Region',
                        link: '/auth/region',
                    },
               
                ],
            },
            {
                text: 'Quản trị người dùng',
                submenu: [
                   
                    {
                        text: 'Thêm và phân quyền người dùng',
                        link: '/auth/quan-tri-nguoi-dung',
                    },
// <<<<<<< HEAD
                    // {
                    //     text: 'Quản trị phân quyền',
                    //     link: '/auth/quan-tri-phan-quyen',
                    // },
                    
               
                ],
            },
            {
                text: 'Quản trị nhóm quyền',
                submenu: [
                   
                    {
                        text: 'Nhóm quyền',
                        link: '/auth/quan-tri-nhom-quyen',
                    },
                     {
                        text: 'Quản trị phân quyền',
                        link: '/auth/quan-tri-phan-quyen',
                    },
                ],
            },
            // {
            //     text: 'Quản trị nhóm quyền',
            //     link: '/auth/quan-tri-nhom-quyen',
            // },
            // {
            //     text: 'Error',
            //     submenu: [
            //         {
            //             text: '401 Page',
            //             link: '/error/401',
            //         },
            //         {
            //             text: '404 Page',
            //             link: '/error/404',
            //         },
            //         {
            //             text: '500 Page',
            //             link: '/error/500',
            //         },
            //     ],
            // },
        ],  
    },
    reports: {
        icon: '',
        text: 'BÁO CÁO ',
        link: '/auth/Embed',
    },
};
