import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { AuthService } from '@modules/auth/services';
import { SBRouteData, SideNavItem } from '@modules/navigation/models';
// import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
    selector: 'sb-side-nav-item',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './side-nav-item.component.html',
    styleUrls: ['side-nav-item.component.scss'],
    // animations: [
    //     trigger('slideInOut', [
    //       state('in', style({
    //         transform: 'translate3d(0, 0, 0)'
    //       })),
    //       state('out', style({
    //         transform: 'translate3d(100%, 0, 0)'
    //       })),
    //       transition('in => out', animate('400ms ease-in-out')),
    //       transition('out => in', animate('400ms ease-in-out'))
    //     ])
    //   ]
})
export class SideNavItemComponent implements OnInit {
    @Input() sideNavItem!: SideNavItem;
    @Input() isActive!: boolean;
    expanded = false;
    routeData!: SBRouteData;

    constructor(private authService: AuthService) {}
    roleDetail: string[] = []
    roleName: string[] = []
    ngOnInit() {
        this.roleDetail = this.authService.getRoleDetail()
        this.roleName = this.authService.getRoleName()
    }
}
