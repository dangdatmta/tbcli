import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { AuthService, UserService } from '@modules/auth/services';
import { DashboardService } from '@modules/dashboard/services';

@Component({
    selector: 'sb-top-nav-user',
    templateUrl: './top-nav-user.component.html',
    styleUrls: ['top-nav-user.component.scss']
})
export class TopNavUserComponent implements OnInit {
    constructor(private authService: AuthService) {}
    userName?:string
    roleDetail: string[] = []
    roleName: string[] = []
    ngOnInit() 
    {
        this.userName = this.authService.getUserName()
        this.roleDetail = this.authService.getRoleDetail()
        this.roleName = this.authService.getRoleName()
    }
}
