namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserBussinessGroup")]
    public partial class UserBussinessGroup
    {
        public int Id { get; set; }

        public int? UserId { get; set; }

        public int? BussinessGroupId { get; set; }

        public BussinessGroup BussinessGroup { get; set; }

        public User User { get; set; }
    }
}
