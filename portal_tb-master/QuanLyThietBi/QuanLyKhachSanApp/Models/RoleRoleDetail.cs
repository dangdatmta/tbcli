namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RoleRoleDetail")]
    public partial class RoleRoleDetail
    {
        public int Id { get; set; }

        public int? RoleId { get; set; }

        public int? RoleDetailId { get; set; }

        public Role Role { get; set; }

        public RoleDetail RoleDetail { get; set; }
    }
}
