namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GroupUser")]
    public partial class GroupUser
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public GroupUser()
        {
            UserGroupUsers = new HashSet<UserGroupUser>();
        }

        public int Id { get; set; }

        [StringLength(150)]
        public string Code { get; set; }

        [StringLength(500)]
        public string Name { get; set; }

        public bool? Status { get; set; }

        [StringLength(150)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedAt { get; set; }

        [StringLength(150)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedAt { get; set; }
        public ICollection<UserGroupUser> UserGroupUsers { get; set; }
    }
}
