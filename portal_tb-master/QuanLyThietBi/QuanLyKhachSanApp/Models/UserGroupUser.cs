namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserGroupUser")]
    public partial class UserGroupUser
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int GroupUserId { get; set; }

        public GroupUser GroupUser { get; set; }

        public User User { get; set; }
    }
}
