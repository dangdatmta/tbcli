namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Role")]
    public partial class Role
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Role()
        {
            RoleRoleDetails = new HashSet<RoleRoleDetail>();
            UserRoles = new HashSet<UserRole>();
        }

        public int Id { get; set; }

        [StringLength(50)]
        public string Code { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        public bool? IsActive { get; set; }

        [StringLength(32)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedAt { get; set; }

        [StringLength(32)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedAt { get; set; }
        public ICollection<RoleRoleDetail> RoleRoleDetails { get; set; }
        public ICollection<UserRole> UserRoles { get; set; }
    }
}
