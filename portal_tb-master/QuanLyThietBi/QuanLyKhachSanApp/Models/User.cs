namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("User")]
    public partial class User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            F_CSKH_05 = new HashSet<F_CSKH_05>();
            F_CSKH05_TRASH = new HashSet<F_CSKH05_TRASH>();
            UserBussinessGroups = new HashSet<UserBussinessGroup>();
            UserGroupUsers = new HashSet<UserGroupUser>();
            UserRoles = new HashSet<UserRole>();
        }

/*        [DatabaseGenerated(DatabaseGeneratedOption.None)]*/
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(10)]
        public string Title { get; set; }

        [StringLength(255)]
        public string UserName { get; set; }

        [StringLength(255)]
        public string Email { get; set; }

        [StringLength(255)]
        public string FirstName { get; set; }

        [StringLength(255)]
        public string MidName { get; set; }

        [StringLength(255)]
        public string LastName { get; set; }

        [StringLength(255)]
        public string FullName { get; set; }

        public int? GroupId { get; set; }

        public int? Status { get; set; }

        [StringLength(500)]
        public string Note { get; set; }

        [StringLength(3000)]
        public string RefreshToken { get; set; }

        [StringLength(255)]
        public string Groups { get; set; }

        [StringLength(45)]
        public string ReceiveEmail { get; set; }

        [StringLength(25)]
        public string Mobile { get; set; }

        [StringLength(25)]
        public string Tel { get; set; }

        [StringLength(255)]
        public string OtherEmail { get; set; }

        [StringLength(2000)]
        public string Avatar { get; set; }

        [StringLength(10)]
        public string Language { get; set; }

        public DateTime? LastLoginAt { get; set; }

        [StringLength(150)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedAt { get; set; }

        [StringLength(150)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedAt { get; set; }

        public bool? IsAdmin { get; set; }

        [StringLength(150)]
        public string GroupBireportId { get; set; }

        [StringLength(50)]
        public string PassWord { get; set; }
        public ICollection<F_CSKH_05> F_CSKH_05 { get; set; }
        public ICollection<F_CSKH05_TRASH> F_CSKH05_TRASH { get; set; }
        public ICollection<UserBussinessGroup> UserBussinessGroups { get; set; }
        public ICollection<UserGroupUser> UserGroupUsers { get; set; }
        public ICollection<UserRole> UserRoles { get; set; }
    }
}
