namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RoleDetail")]
    public partial class RoleDetail
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RoleDetail()
        {
            RoleRoleDetails = new HashSet<RoleRoleDetail>();
        }

        public int Id { get; set; }

        [StringLength(50)]
        public string Code { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        public bool? IsActive { get; set; }

        [StringLength(32)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedAt { get; set; }

        [StringLength(32)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedAt { get; set; }

        public ICollection<RoleRoleDetail> RoleRoleDetails { get; set; }
    }
}
