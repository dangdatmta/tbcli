using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace QuanLyKhachSanApp.Models
{
    public partial class TCEntities : DbContext
    {
        public TCEntities()
            : base("name=TCEntities")
        {
        }

        public virtual DbSet<BussinessGroup> BussinessGroups { get; set; }
        public virtual DbSet<F_CSKH_05> F_CSKH_05 { get; set; }
        public virtual DbSet<F_CSKH05_TRASH> F_CSKH05_TRASH { get; set; }
        public virtual DbSet<GroupUser> GroupUsers { get; set; }
        public virtual DbSet<Region> Regions { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<RoleDetail> RoleDetails { get; set; }
        public virtual DbSet<RoleRoleDetail> RoleRoleDetails { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserBussinessGroup> UserBussinessGroups { get; set; }
        public virtual DbSet<UserGroupUser> UserGroupUsers { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BussinessGroup>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<BussinessGroup>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<BussinessGroup>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<F_CSKH_05>()
                .Property(e => e.CompanyCode)
                .IsUnicode(false);

            modelBuilder.Entity<F_CSKH_05>()
                .Property(e => e.MAKY)
                .IsUnicode(false);

            modelBuilder.Entity<F_CSKH05_TRASH>()
                .Property(e => e.CompanyCode)
                .IsUnicode(false);

            modelBuilder.Entity<F_CSKH05_TRASH>()
                .Property(e => e.MAKY)
                .IsUnicode(false);

            modelBuilder.Entity<GroupUser>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<GroupUser>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<GroupUser>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<GroupUser>()
                .HasMany(e => e.UserGroupUsers)
                .WithRequired(e => e.GroupUser)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Region>()
                .Property(e => e.AreaId)
                .IsUnicode(false);

            modelBuilder.Entity<Region>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<Region>()
                .Property(e => e.RegionPath)
                .IsUnicode(false);

            modelBuilder.Entity<Region>()
                .Property(e => e.ParentId)
                .IsUnicode(false);

            modelBuilder.Entity<Region>()
                .Property(e => e.BiCode)
                .IsUnicode(false);

            modelBuilder.Entity<Region>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<Region>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<Role>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<Role>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<Role>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<RoleDetail>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<RoleDetail>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<RoleDetail>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.UserName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.MidName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.RefreshToken)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Groups)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.ReceiveEmail)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Mobile)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Tel)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.OtherEmail)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Avatar)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Language)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.GroupBireportId)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.PassWord)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.F_CSKH_05)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.idUser);

            modelBuilder.Entity<User>()
                .HasMany(e => e.F_CSKH05_TRASH)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.idUser);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UserGroupUsers)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);
        }
    }
}
