namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("[F_CSKH.05]")]
    public partial class F_CSKH_05
    {
        public int Id { get; set; }

        [StringLength(150)]
        public string CompanyCode { get; set; }

        [StringLength(150)]
        public string MAKY { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        [StringLength(250)]
        public string CreateUser { get; set; }

        [StringLength(250)]
        public string UpdateUser { get; set; }

        [StringLength(250)]
        public string CHITIEUD { get; set; }

        public double? TUONGQUAN { get; set; }

        public double? TRONGSO { get; set; }

        public int? idUser { get; set; }

        public int? version { get; set; }

        public User User { get; set; }
    }
}
