namespace QuanLyKhachSanApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Region")]
    public partial class Region
    {
        public int Id { get; set; }

        [Required]
        [StringLength(32)]
        public string AreaId { get; set; }

        [Required]
        [StringLength(50)]
        public string Code { get; set; }

        [StringLength(250)]
        public string Name { get; set; }

        public int? RegionLevel { get; set; }

        [StringLength(500)]
        public string RegionPath { get; set; }

        [StringLength(32)]
        public string ParentId { get; set; }

        [StringLength(255)]
        public string BiCode { get; set; }

        [StringLength(150)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedAt { get; set; }

        [StringLength(150)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedAt { get; set; }

        public bool? Status { get; set; }
    }
}
