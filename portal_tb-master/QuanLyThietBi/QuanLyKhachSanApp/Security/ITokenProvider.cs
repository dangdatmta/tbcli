﻿using System;
namespace HVIT.Security
{
    public interface ITokenProvider
    {
        bool ValidateToken(ref TokenIdentity tokenIdentity);
        TokenIdentity GenerateToken(Guid userId, string username, string userAgent, string ip, string guid, long effectiveTime);
    }
}
