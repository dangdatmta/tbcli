﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using CMS.Models;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/phieughitang")]
    public class PhieuGhiTangController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> Search([FromUri] Pagination pagination, [FromUri] string keywords = null)
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<PhieuGhiTang> results = db.PhieuGhiTang;
                if (pagination == null)
                    pagination = new Pagination();
                if (pagination.includeEntities)
                {
                    results = results.Include(x => x.NguoiLapPhieu);
                }

                if (!string.IsNullOrWhiteSpace(keywords))
                    results = results.Where(x => x.MaPhieu  .Contains(keywords));

                results = results.OrderBy(o => o.PhieuGhiTangID);

                return Ok((await GetPaginatedResponse(results, pagination)));   
            }
        } 

        [HttpGet, Route("{phieuGhiTangID:int}")]
        public async Task<IHttpActionResult> Get(int phieuGhiTangID)
        {
            using (var db = new ApplicationDbContext())
            {
                var phieuGhiTang = await db.PhieuGhiTang
                    .SingleOrDefaultAsync(o => o.PhieuGhiTangID == phieuGhiTangID);

                if (phieuGhiTang == null)
                    return NotFound();

                return Ok(phieuGhiTang);
            }
        }

        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Insert([FromBody] PhieuGhiTang phieuGhiTang)
        {
            if (phieuGhiTang.PhieuGhiTangID != 0) return BadRequest("Invalid PhieuGhiTangID");

            using (var db = new ApplicationDbContext())
            {
                //List<ThietBi> thietBis = TaoDanhSachThietBiTuPhieuGhiTang(phieuGhiTang.ChiTietPhieu);
                await XuLyDauVao(TaoDanhSachThietBiTuPhieuGhiTang(phieuGhiTang.ChiTietPhieu), null);

                // thêm vào kho
                //db.ThietBi.AddRange(thietBis);

                // ghi vào sổ

                db.PhieuGhiTang.Add(phieuGhiTang);

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (Exception Ex)
                {
                    throw Ex;
                } 
            }

            return Ok(phieuGhiTang);
        }

        [HttpPut, Route("{phieuGhiTangID:int}")]
        public async Task<IHttpActionResult> Update(int phieuGhiTangID, [FromBody] PhieuGhiTang phieuGhiTang)
        {
            if (phieuGhiTang.PhieuGhiTangID != phieuGhiTangID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new ApplicationDbContext())
            {
                var phieuGhiTangOld = db.PhieuGhiTang.AsNoTracking().FirstOrDefault(o => o.PhieuGhiTangID == phieuGhiTangID);
                List<ThietBi> thietBiOld = TaoDanhSachThietBiTuPhieuGhiTang(phieuGhiTangOld.ChiTietPhieu);
                List<ThietBi> thietBiNew= TaoDanhSachThietBiTuPhieuGhiTang(phieuGhiTang.ChiTietPhieu);

                await XuLyDauVao(thietBiNew, thietBiOld);

                db.Entry(phieuGhiTang).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.PhieuGhiTang.Count(o => o.PhieuGhiTangID == phieuGhiTangID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(phieuGhiTang);
            }
        }

        [HttpDelete, Route("{phieuGhiTangID:int}")]
        public async Task<IHttpActionResult> Delete(int phieuGhiTangID)
        {
            using (var db = new ApplicationDbContext())
            {
                var phieuGhiTang = await db.PhieuGhiTang.SingleOrDefaultAsync(o => o.PhieuGhiTangID == phieuGhiTangID);

                if (phieuGhiTang == null)
                    return NotFound();

                await XuLyDauVao(null, TaoDanhSachThietBiTuPhieuGhiTang(phieuGhiTang.ChiTietPhieu));

                db.Entry(phieuGhiTang).State = EntityState.Deleted;
                await db.SaveChangesAsync();
                return Ok();
            }
        }

        private List<ThietBi> TaoDanhSachThietBiTuPhieuGhiTang(string jsonChiTietGhiTang)
        {
            var chiTietGhiTang = JsonConvert.DeserializeObject<List<ChiTietPhieuGhiTang>>(jsonChiTietGhiTang);
            List<ThietBi> chiTietGhiKho = new List<ThietBi>();
            // bẻ ra danh sách thiết bị kho 
            for (var i = 0; i < chiTietGhiTang.Count(); i++)
            {
                if (chiTietGhiTang[i].LaQuanLyChiTiet == true)
                {
                    List<string> lstCode = new List<string>();
                    // Cắt chém số hiệu
                    var codeReverse = new string(chiTietGhiTang[i].SoHieuTu.Reverse().ToArray());

                    int pos = Regex.Match(codeReverse, "[A-Za-z]").Index;
                    string number = new string(codeReverse.Substring(0, pos).Reverse().ToArray());
                    string character = new string(codeReverse.Substring(pos, chiTietGhiTang[i].SoHieuTu.Length - number.Length).Reverse().ToArray());

                    for (int j = int.Parse(number); j < int.Parse(number) + chiTietGhiTang[i].SoLuong; j++)
                    {
                        lstCode.Add(character + string.Concat(Enumerable.Repeat("0", number.Length - j.ToString().Length)) + j.ToString());
                        ThietBi temp = new ThietBi();
                        temp.DanhMucThietBiID = chiTietGhiTang[i].DanhMucThietBiID;
                        temp.DonViID = chiTietGhiTang[i].DonViID;
                        temp.HanSuDung = chiTietGhiTang[i].HanSuDung;
                        temp.KhoPhongID = chiTietGhiTang[i].KhoPhongID;
                        temp.MaThietBi = chiTietGhiTang[i].MaThietBi;
                        temp.NoiSanXuat = chiTietGhiTang[i].NoiSanXuat;
                        temp.SoLuong = 1;
                        temp.TenThietBi = chiTietGhiTang[i].TenThietBi;
                        temp.TenLoaiThietBi = chiTietGhiTang[i].TenLoaiThietBi;
                        temp.DonViTinh = chiTietGhiTang[i].DonViTinh;
                        temp.SoHieuThietBi = character + string.Concat(Enumerable.Repeat("0", number.Length - j.ToString().Length)) + j.ToString();
                        chiTietGhiKho.Add(temp);
                    }
                }
                else
                {
                    ThietBi temp = new ThietBi();
                    temp.DanhMucThietBiID = chiTietGhiTang[i].DanhMucThietBiID;
                    temp.DonViID = chiTietGhiTang[i].DonViID;
                    temp.HanSuDung = chiTietGhiTang[i].HanSuDung;
                    temp.KhoPhongID = chiTietGhiTang[i].KhoPhongID;
                    temp.MaThietBi = chiTietGhiTang[i].MaThietBi;
                    temp.NoiSanXuat = chiTietGhiTang[i].NoiSanXuat;
                    temp.SoLuong = chiTietGhiTang[i].SoLuong;
                    temp.TenThietBi = chiTietGhiTang[i].TenThietBi;
                    temp.TenLoaiThietBi = chiTietGhiTang[i].TenLoaiThietBi;
                    temp.DonViTinh = chiTietGhiTang[i].DonViTinh;
                    temp.SoHieuThietBi = chiTietGhiTang[i].SoHieu;

                    chiTietGhiKho.Add(temp);
                }
            }
            return chiTietGhiKho;
        }

        public async Task<int> XuLyDauVao(List<ThietBi> thietBiNew, List<ThietBi> thietBiOld)
        {
            using (var db = new ApplicationDbContext())
            {
                // danh sách thêm mới sau khi sửa
                List<ThietBi> lstNew = new List<ThietBi>();
                if (thietBiNew != null)
                {
                    thietBiNew.ForEach(x =>
                    {
                        var item = db.ThietBi.FirstOrDefault(t => t.DanhMucThietBiID == x.DanhMucThietBiID && t.SoHieuThietBi == x.SoHieuThietBi && t.KhoPhongID == x.KhoPhongID);
                        if (item != null)
                        {
                            item.SoLuong += x.SoLuong;
                        }
                        else 
                        {
                            var count = 0;
                            if (thietBiOld != null)
                            {
                                thietBiOld.ForEach(y =>
                                {
                                    if (x.DanhMucThietBiID == y.DanhMucThietBiID && x.SoHieuThietBi == y.SoHieuThietBi && x.KhoPhongID == y.KhoPhongID)
                                        count++;
                                });
                            }
                            if (count == 0)
                                lstNew.Add(x);
                        }
                    });
                }

                // danh sách cần xóa sau khi sửa
                List<ThietBi> lstDelete = new List<ThietBi>();
                if(thietBiOld != null)
                {
                    thietBiOld.ForEach(x =>
                    {
                        var item = db.ThietBi.FirstOrDefault(t => t.DanhMucThietBiID == x.DanhMucThietBiID && t.SoHieuThietBi == x.SoHieuThietBi && t.KhoPhongID == x.KhoPhongID);
                        if (item != null)
                        {
                            var count = 0;
                            item.SoLuong -= x.SoLuong;

                            if (thietBiNew != null)
                            {
                                thietBiNew.ForEach(y =>
                                {
                                    if (x.DanhMucThietBiID == y.DanhMucThietBiID && x.SoHieuThietBi == y.SoHieuThietBi)
                                        count++;
                                });
                            }
                            if (count == 0 && item.SoLuong == 0)
                                lstDelete.Add(item);
                        }
                    });
                }

                db.ThietBi.AddRange(lstNew);
                db.ThietBi.RemoveRange(lstDelete);

                await db.SaveChangesAsync();
                return 1;
            }
        }
    }
}
