﻿using HVIT.Security;
using HVITCore.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Data.Entity;
using CMS.Models;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/auth")]
    public class AuthController : BaseApiController
    {
        public class LoginForm
        {
            public string Username { get; set; }
            public string Password { get; set; }
            public string RePassword { get; set; }
            public string ChangePasswordKey { get; set; }
            public string OldPassword { get; set; }
        }

        public class UserModel
        {
            public Users User { get; set; }
            public List<int> Groups { get; set; }

            public UserModel()
            {
                Groups = new List<int>();
            }
        }

        [HttpPost]
        [Route("login")]
        public IHttpActionResult Login(LoginForm login)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                Users user = db.Users.SingleOrDefault(x => x.UserName == login.Username);
                if (user != null)
                {
                    string passwordSalt = user.PasswordSalt;
                    string passwordInput = AuthenticationHelper.GetMd5Hash(passwordSalt + login.Password);
                    string passwordUser = user.Password;

                    if (passwordInput.Equals(passwordUser))
                    {
                        TokenProvider tokenProvider = new TokenProvider();
                        TokenIdentity token = tokenProvider.GenerateToken(user.UserID, login.Username,
                            Request.Headers.UserAgent.ToString(),
                            "", Guid.NewGuid().ToString(),
                            DateTime.Now.Ticks);
                        token.SetAuthenticationType("Custom");
                        token.SetIsAuthenticated(true);
                        db.AccessTokens.Add(new AccessTokens()
                        {
                            Token = token.Token,
                            EffectiveTime = new DateTime(token.EffectiveTime),
                            ExpiresIn = token.ExpiresTime,
                            IP = token.IP,
                            UserAgent = token.UserAgent,
                            UserName = token.Name
                        });
                        db.SaveChanges();
                        var giaoVien = db.GiaoVien 
                            .Select(x => new
                            {
                                x.GiaoVienID,
                                x.UserID, 
                                x.TenDayDu,
                                x.SoDienThoai
                            })
                            .FirstOrDefault(x => x.UserID == user.UserID);
                        return Ok(
                            new
                            {
                                AccessToken = token,
                                Profile = new
                                {
                                    UserId = user.UserID,
                                    Username = user.UserName,
                                    //NhanVien = nhanVien,
                                }
                            }
                        );
                    }
                }
                return Ok("Login failed!");
            }
        }

        [AuthorizeUser, HttpGet]
        [Route("logout")]
        public IHttpActionResult Logout(Guid UserID)
        {
            using (var db = new ApplicationDbContext())
            {
                var user = db.Users.Where(x => x.UserID == UserID).FirstOrDefault();
                if (user == null)
                    return BadRequest("invalid UserID");
                db.SaveChanges();
                return Ok();
            }
        }
        [AuthorizeUser, HttpGet]
        [Route("validate-token")]
        public IHttpActionResult ValidateToken()
        {
            TokenIdentity tokenIdentity = ClaimsPrincipal.Current.Identity as TokenIdentity;
            return Ok();
        }
        
    }
    public class AllowCrossSiteJsonAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            filterContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            base.OnActionExecuting(filterContext);
        }
    }
}
