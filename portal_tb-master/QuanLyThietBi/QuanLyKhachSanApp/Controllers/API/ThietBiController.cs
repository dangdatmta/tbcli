using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using CMS.Models;
using System.Collections.Generic;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/thietbi")]
    public class ThietBiController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> Search([FromUri] Pagination pagination, [FromUri] string keywords = null)
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<ThietBi> results = db.ThietBi;
                if (pagination == null)
                    pagination = new Pagination();
                if (pagination.includeEntities)
                {
                    results = results.Include(x => x.KhoPhong).Include(x => x.KhoPhong.GiaoVien);
                }

                if (!string.IsNullOrWhiteSpace(keywords))
                    results = results.Where(x => x.TenThietBi.Contains(keywords));

                results = results.OrderBy(x => x.ThietBiID);

                return Ok((await GetPaginatedResponse(results, pagination)));
            }
        } 

        [HttpGet, Route("{thietBiID:int}")]
        public async Task<IHttpActionResult> Get(int thietBiID)
        {
            using (var db = new ApplicationDbContext())
            {
                var thietBi = await db.ThietBi
                    .SingleOrDefaultAsync(o => o.ThietBiID == thietBiID);

                if (thietBi == null)
                    return NotFound(); 
                return Ok(thietBi);
            }
        }

        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Insert([FromBody] ThietBi thietBi)
        {
            if (thietBi.ThietBiID != 0) return BadRequest("Invalid ThietBiID");

            using (var db = new ApplicationDbContext())
            { 
                //thietBi.ThietBiBoMon.ToList().ForEach(x => x.ThietBi = null);
                db.ThietBi.Add(thietBi);
                await db.SaveChangesAsync();
            }

            return Ok(thietBi);
        }

        [HttpPut, Route("{thietBiID:int}")]
        public async Task<IHttpActionResult> Update(int thietBiID, [FromBody] ThietBi thietBi)
        {
            if (thietBi.ThietBiID != thietBiID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new ApplicationDbContext())
            { 
                db.Entry(thietBi).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.ThietBi.Count(o => o.ThietBiID == thietBiID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(thietBi);
            }
        }

        [HttpDelete, Route("{thietBiID:int}")]
        public async Task<IHttpActionResult> Delete(int thietBiID)
        {
            using (var db = new ApplicationDbContext())
            {
                var thietBi = await db.ThietBi.SingleOrDefaultAsync(o => o.ThietBiID == thietBiID);

                if (thietBi == null)
                    return NotFound();

                db.Entry(thietBi).State = EntityState.Deleted;
                await db.SaveChangesAsync();
                return Ok();
            }
        }

    }
}
