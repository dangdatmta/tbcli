﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using System.Net.Http;
using System.Web;
using QuanLyKhachSanApp.Models;
using System.IO;
using ExcelDataReader;
using System.Collections.Generic;
using System.Data;
using System.Net;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/excel")]
    public class ImportExcelController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> Get()
        {
            using (var db = new TCEntities())
            {
                //List<F_CSKH_05> bc = new List<F_CSKH_05>();
                //var ver = await db.F_CSKH_05.ToListAsync();
                List<int?> ver = await db.F_CSKH_05.Select(x => x.version).Distinct().ToListAsync();
                //bc = await db.F_CSKH_05.GroupBy(x=>x.version).Select(g=>g.FirstOrDefault()).ToListAsync();

                if (ver == null)
                    return NotFound();

                return Ok(ver);
            }
        }
        [HttpGet, Route("{version}")]
        public async Task<IHttpActionResult> GetByVersion(int version)
        {
            using (var db = new TCEntities())
            {
                var bc = await db.F_CSKH_05.Where(o => o.version == version).ToListAsync();
                //GroupBy(p=>p.CreateDate).ToListAsync();

                if (bc == null)
                    return NotFound();

                return Ok(bc);
            }
        }
        /*        [HttpGet, Route("{version}")]
                public async Task<IHttpActionResult> GetByVersion()
                {
                    using (var db = new TCEntities())
                    {
                       *//* List<int?> listVer = await db.F_CSKH_05.Select(x => x.version).Distinct().ToListAsync();
                        List<F_CSKH_05> bc = new List<F_CSKH_05>();
                        List<F_CSKH_05> bcReal = new List<F_CSKH_05>();
                        foreach (var ver in listVer)
                        {
                            bc = await db.F_CSKH_05.Where(o => o.version == ver).ToListAsync();
                            bcReal.AddRange(bc);
                        }

                        //GroupBy(p=>p.CreateDate).ToListAsync();

                        if (bcReal == null)
                            return NotFound();

                        return Ok(bcReal);*//*
                    }
                }*/

        [HttpPost, Route("")]
        public string ExcelUpload()
        {
            string message = "";
            HttpResponseMessage result = null;
            var httpRequest = HttpContext.Current.Request;
            using (var objEntity = new TCEntities())
            {

                if (httpRequest.Files.Count > 0)
                {
                    HttpPostedFile file = httpRequest.Files[0];
                    Stream stream = file.InputStream;

                    IExcelDataReader reader = null;

                    if (file.FileName.EndsWith(".xls"))
                    {
                        reader = ExcelReaderFactory.CreateBinaryReader(stream);
                    }
                    else if (file.FileName.EndsWith(".xlsx"))
                    {
                        reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    }
                    else
                    {
                        message = "This file format is not supported";
                    }

                    DataSet excelRecords = reader.AsDataSet();
                    reader.Close();

                    var verMax = objEntity.F_CSKH_05.Max(x=>x.version);
                    var finalRecords = excelRecords.Tables[0];
                    for (int i = 0; i < finalRecords.Rows.Count; i++)
                    {
                        F_CSKH_05 objUser = new F_CSKH_05();
                        objUser.CompanyCode = finalRecords.Rows[i][0].ToString();
                        objUser.MAKY = finalRecords.Rows[i][1].ToString();
                        objUser.CreateDate = DateTime.Now;
                        objUser.UpdateDate = DateTime.Parse(finalRecords.Rows[i][3].ToString());
                        objUser.CreateUser = finalRecords.Rows[i][4].ToString();
                        objUser.UpdateUser = finalRecords.Rows[i][5].ToString();
                        objUser.CHITIEUD = finalRecords.Rows[i][6].ToString();
                        objUser.TUONGQUAN = Double.Parse(finalRecords.Rows[i][7].ToString()); 
                        objUser.TRONGSO = Double.Parse(finalRecords.Rows[i][8].ToString());
                        objUser.version = verMax + 1;
                        objEntity.F_CSKH_05.Add(objUser);

                    }

                    int output = objEntity.SaveChanges();
                    if (output > 0)
                    {
                        message = "Excel file has been successfully uploaded";
                    }
                    else
                    {
                        message = "Excel file uploaded has faild";
                    }

                }

                else
                {
                    result = Request.CreateResponse(HttpStatusCode.BadRequest);
                }
            }
            return message;
        }
    }
}
