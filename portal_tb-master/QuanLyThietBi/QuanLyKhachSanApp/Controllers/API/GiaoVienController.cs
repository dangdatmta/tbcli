using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using CMS.Models;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/giaovien")]
    public class GiaoVienController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> Search([FromUri] Pagination pagination, [FromUri] string keywords = null)
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<GiaoVien> results = db.GiaoVien;
                if (pagination == null)
                    pagination = new Pagination();
                if (!pagination.includeEntities)
                { 
                }

                if (!string.IsNullOrWhiteSpace(keywords))
                    results = results.Where(x => x.TenDayDu.Contains(keywords));

                results = results.OrderBy(o => o.GiaoVienID);

                return Ok((await GetPaginatedResponse(results, pagination)));
            }
        }
         
        [HttpGet, Route("{giaoVienID:int}")]
        public async Task<IHttpActionResult> Get(int giaoVienID)
        {
            using (var db = new ApplicationDbContext())
            {
                var giaoVien = await db.GiaoVien
                    .SingleOrDefaultAsync(o => o.GiaoVienID == giaoVienID);

                if (giaoVien == null)
                    return NotFound();

                return Ok(giaoVien);
            }
        }

        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Insert([FromBody] GiaoVien giaoVien)
        {
            if (giaoVien.GiaoVienID != 0) return BadRequest("Invalid GiaoVienID");

            using (var db = new ApplicationDbContext())
            {
                db.GiaoVien.Add(giaoVien);
                await db.SaveChangesAsync();
            }

            return Ok(giaoVien);
        }

        [HttpPut, Route("{giaoVienID:int}")]
        public async Task<IHttpActionResult> Update(int giaoVienID, [FromBody] GiaoVien giaoVien)
        {
            if (giaoVien.GiaoVienID != giaoVienID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new ApplicationDbContext())
            {
                db.Entry(giaoVien).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.GiaoVien.Count(o => o.GiaoVienID == giaoVienID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(giaoVien);
            }
        }

        [HttpDelete, Route("{giaoVienID:int}")]
        public async Task<IHttpActionResult> Delete(int giaoVienID)
        {
            using (var db = new ApplicationDbContext())
            {
                var giaoVien = await db.GiaoVien.SingleOrDefaultAsync(o => o.GiaoVienID == giaoVienID);

                if (giaoVien == null)
                    return NotFound();

                db.Entry(giaoVien).State = EntityState.Deleted;
                await db.SaveChangesAsync();
                return Ok();
            }
        }

    }
}
