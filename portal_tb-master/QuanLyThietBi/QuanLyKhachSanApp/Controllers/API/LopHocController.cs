using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using CMS.Models;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/lophoc")]
    public class LopHocController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> Search([FromUri] Pagination pagination, [FromUri] string keywords = null)
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<LopHoc> results = db.LopHoc;
                if (pagination == null)
                    pagination = new Pagination();
                if (pagination.includeEntities)
                {
                    results = results.Include(x => x.KhoiHoc);
                }

                if (!string.IsNullOrWhiteSpace(keywords))
                    results = results.Where(x => x.TenLop.Contains(keywords));

                results = results.OrderBy(o => o.LopHocID);

                return Ok((await GetPaginatedResponse(results, pagination)));
            }
        } 

        [HttpGet, Route("{lopHocID:int}")]
        public async Task<IHttpActionResult> Get(int lopHocID)
        {
            using (var db = new ApplicationDbContext())
            {
                var lopHoc = await db.LopHoc
                    .SingleOrDefaultAsync(o => o.LopHocID == lopHocID);

                if (lopHoc == null)
                    return NotFound();

                return Ok(lopHoc);
            }
        }

        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Insert([FromBody] LopHoc lopHoc)
        {
            if (lopHoc.LopHocID != 0) return BadRequest("Invalid LopHocID");

            using (var db = new ApplicationDbContext())
            {
                db.LopHoc.Add(lopHoc);
                await db.SaveChangesAsync();
            }

            return Ok(lopHoc);
        }

        [HttpPut, Route("{lopHocID:int}")]
        public async Task<IHttpActionResult> Update(int lopHocID, [FromBody] LopHoc lopHoc)
        {
            if (lopHoc.LopHocID != lopHocID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new ApplicationDbContext())
            {
                db.Entry(lopHoc).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.LopHoc.Count(o => o.LopHocID == lopHocID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(lopHoc);
            }
        }

        [HttpDelete, Route("{lopHocID:int}")]
        public async Task<IHttpActionResult> Delete(int lopHocID)
        {
            using (var db = new ApplicationDbContext())
            {
                var lopHoc = await db.LopHoc.SingleOrDefaultAsync(o => o.LopHocID == lopHocID);

                if (lopHoc == null)
                    return NotFound();

                db.Entry(lopHoc).State = EntityState.Deleted;
                await db.SaveChangesAsync();
                return Ok();
            }
        }

    }
}
