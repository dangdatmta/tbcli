﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using QuanLyKhachSanApp.Models;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/role")]
    public class RoleController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            using (var db = new TCEntities())
            {
                var role = await db.Roles.ToListAsync();
                if (role == null)
                    return NotFound();
                return Ok(role);
            }
        }

        [HttpGet, Route("{roleID}")]
        public async Task<IHttpActionResult> Get(int roleID)
        {
            using (var db = new TCEntities())
            {
                var role = await db.Roles
                    .SingleOrDefaultAsync(o => o.Id == roleID);

                if (role == null)
                    return NotFound();

                return Ok(role);
            }
        }

        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Insert([FromBody] Role role)
        {
            using (var db = new TCEntities())
            {
                db.Roles.Add(role);
                await db.SaveChangesAsync();
            }
            return Ok(role);
        }

        [HttpPut, Route("{roleID}")]
        public async Task<IHttpActionResult> Update(int roleID, [FromBody] Role role)
        {
            if (role.Id != roleID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new TCEntities())
            {
                db.Entry(role).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.Roles.Count(o => o.Id == roleID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(role);
            }
        }

        [HttpDelete, Route("{roleID}")]
        public async Task<String> Delete(int roleID)
        {
            string message = "";
            using (var db = new TCEntities())
            {
                var role = await db.Roles.SingleOrDefaultAsync(o => o.Id == roleID);
                var userRole = await db.UserRoles.Where(o => o.RoleId == roleID).ToListAsync();
                if (role == null){
                    message = "Không tìm thấy nhóm quyền!!!";
                }
                else
                {
                    foreach (var ur in userRole){
                        db.Entry(ur).State = EntityState.Deleted;
                    }
                    db.Entry(role).State = EntityState.Deleted;
                    int output = await db.SaveChangesAsync();
                    if (output > 0){
                        message = "Xóa thành công quyền";
                    }
                    else{
                        message = "Có lỗi xảy ra!!!";
                    }
                }
                return message;
            }
        }

    }
}
