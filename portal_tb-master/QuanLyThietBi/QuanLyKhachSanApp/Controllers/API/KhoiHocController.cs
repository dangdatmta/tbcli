using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using CMS.Models;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/khoihoc")]
    public class KhoiHocController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> Search([FromUri] Pagination pagination, [FromUri] string keywords = null)
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<KhoiHoc> results = db.KhoiHoc;
                if (pagination == null)
                    pagination = new Pagination();
                if (!pagination.includeEntities)
                { 
                }

                if (!string.IsNullOrWhiteSpace(keywords))
                    results = results.Where(x => x.TenKhoi.Contains(keywords));

                results = results.OrderBy(o => o.KhoiID);

                return Ok((await GetPaginatedResponse(results, pagination)));
            }
        } 

        [HttpGet, Route("{khoiHocID:int}")]
        public async Task<IHttpActionResult> Get(int khoiHocID)
        {
            using (var db = new ApplicationDbContext())
            {
                var khoiHoc = await db.KhoiHoc
                    .SingleOrDefaultAsync(o => o.KhoiID == khoiHocID);

                if (khoiHoc == null)
                    return NotFound();

                return Ok(khoiHoc);
            }
        }

        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Insert([FromBody] KhoiHoc khoiHoc)
        {
            if (khoiHoc.KhoiID != 0) return BadRequest("Invalid KhoiHocID");

            using (var db = new ApplicationDbContext())
            {
                db.KhoiHoc.Add(khoiHoc);
                await db.SaveChangesAsync();
            }

            return Ok(khoiHoc);
        }

        [HttpPut, Route("{khoiHocID:int}")]
        public async Task<IHttpActionResult> Update(int khoiHocID, [FromBody] KhoiHoc khoiHoc)
        {
            if (khoiHoc.KhoiID != khoiHocID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new ApplicationDbContext())
            {
                db.Entry(khoiHoc).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.KhoiHoc.Count(o => o.KhoiID == khoiHocID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(khoiHoc);
            }
        }

        [HttpDelete, Route("{khoiHocID:int}")]
        public async Task<IHttpActionResult> Delete(int khoiHocID)
        {
            using (var db = new ApplicationDbContext())
            {
                var khoiHoc = await db.KhoiHoc.SingleOrDefaultAsync(o => o.KhoiID == khoiHocID);

                if (khoiHoc == null)
                    return NotFound();

                db.Entry(khoiHoc).State = EntityState.Deleted;
                await db.SaveChangesAsync();
                return Ok();
            }
        }

    }
}
