﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using QuanLyKhachSanApp.Models;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/region")]
    public class VungMienController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            using (var db = new TCEntities())
            {
                var region = await db.Regions.ToListAsync();
                if (region == null)
                    return NotFound();
                return Ok(region);
            }
        }

        [HttpGet, Route("{regionID}")]
        public async Task<IHttpActionResult> Get(int regionID)
        {
            using (var db = new TCEntities())
            {
                var region = await db.Regions
                    .SingleOrDefaultAsync(o => o.Id == regionID);

                if (region == null)
                    return NotFound();

                return Ok(region);
            }
        }

        /*[HttpPost, Route("")]
        public async Task<IHttpActionResult> Insert([FromBody] MonHoc monHoc)
        {
            if (monHoc.MonHocID != 0) return BadRequest("Invalid MonHocID");

            using (var db = new ApplicationDbContext())
            {
                db.MonHoc.Add(monHoc);
                await db.SaveChangesAsync();
            }

            return Ok(monHoc);
        }

        [HttpPut, Route("{monHocID:int}")]
        public async Task<IHttpActionResult> Update(int monHocID, [FromBody] MonHoc monHoc)
        {
            if (monHoc.MonHocID != monHocID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new ApplicationDbContext())
            {
                db.Entry(monHoc).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.MonHoc.Count(o => o.MonHocID == monHocID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(monHoc);
            }
        }

        [HttpDelete, Route("{monHocID:int}")]
        public async Task<IHttpActionResult> Delete(int monHocID)
        {
            using (var db = new ApplicationDbContext())
            {
                var monHoc = await db.MonHoc.SingleOrDefaultAsync(o => o.MonHocID == monHocID);

                if (monHoc == null)
                    return NotFound();

                db.Entry(monHoc).State = EntityState.Deleted;
                await db.SaveChangesAsync();
                return Ok();
            }
        }*/

    }
}
