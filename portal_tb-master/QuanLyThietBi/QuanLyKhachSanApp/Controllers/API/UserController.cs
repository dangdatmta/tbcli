﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;
using QuanLyKhachSanApp.Models;

namespace QuanLyKhachSanApp.Controllers.API
{
    [RoutePrefix("api/user")]
    public class UserController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            using (var db = new TCEntities())
            {
                var user = await db.Users.Include(x => x.UserRoles).Include("UserRoles.Role").ToListAsync();
               
                //var user = await db.Users.Include("UserRole").ToListAsync();
                if (user == null)
                    return NotFound();
                return Ok(user);
            }
        }

        [HttpGet, Route("{userId}")]
        public async Task<IHttpActionResult> GetById(int userId)
        {
            using (var db = new TCEntities())
            {
                var user = await db.Users.Include(x => x.UserBussinessGroups).Include("UserBussinessGroups.BussinessGroup").SingleOrDefaultAsync(o => o.Id == userId);
                if (user == null)
                    return NotFound();
                return Ok(user);
            }
        }

        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Insert([FromBody] User user)
        {
            using (var db = new TCEntities())
            {
                db.Users.Add(user);
                await db.SaveChangesAsync();

                // UserRole userRole = new UserRole();
                // Random rand = new Random();
                // user.Id = rand.Next(1, 1000000).ToString();      
                // db.Users.Add(user);
                // await db.SaveChangesAsync();
                // userRole.Id = rand.Next(1, 1000000).ToString();
                // userRole.UserId = user.Id;

                // //var UR = db
                //// foreach ()
                // //{
                //     //userRole.RoleId = "5b86edcb09ec4453895bf55620605214";
                //     db.UserRoles.Add(userRole);
                //     await db.SaveChangesAsync();
                // //}
            }
            return Ok(user);
        }

        [HttpPut, Route("{userId}")]
        public async Task<IHttpActionResult> Update(int userID, [FromBody] User user)
        {
            if (user.Id != userID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new TCEntities())
            {
                /*var roleId = await db.Users.Include(o => o.UserRole).Include("UserRole.Role").Where(o => o.Id == userID).ToListAsync();
                foreach (var rID in roleId)
                {

                }*/
                db.Entry(user).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    bool exists = db.Users.Count(o => o.Id == userID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ex;
                    }
                }

                return Ok(user);
            }
        }

        [HttpDelete, Route("{userId}")]
        public async Task<String> Delete(int userID)
        {
            string message = "";
            using (var db = new TCEntities())
            {
                var user = await db.Users.SingleOrDefaultAsync(o => o.Id == userID);
                var userRoles = await db.UserRoles.Where(o => o.UserId == userID).ToListAsync();
                var userBSN = await db.UserBussinessGroups.Where(o => o.UserId == userID).ToListAsync();
                var userGrU = await db.UserGroupUsers.Where(o => o.UserId == userID).ToListAsync();
                if (user == null) { 
                    message = "Không tìm thấy User";
                }
                else
                {
                    foreach (var UGR in userGrU)
                    {
                        db.Entry(UGR).State = EntityState.Deleted;
                    }
                    foreach (var UBG in userBSN)
                    {
                        db.Entry(UBG).State = EntityState.Deleted;
                    }
                    foreach (var UR in userRoles)
                    {
                        db.Entry(UR).State = EntityState.Deleted;
                    }
                    db.Entry(user).State = EntityState.Deleted;
                    int output = await db.SaveChangesAsync();
                    if (output > 0)
                    {
                        message = "Xóa người dùng thành công";
                    }
                    else
                    {
                        message = "Có lỗi xảy ra!!!";
                    }
                }

                return message;
            }
        }
    }
}