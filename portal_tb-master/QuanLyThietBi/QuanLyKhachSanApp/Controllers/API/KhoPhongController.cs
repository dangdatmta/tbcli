using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using CMS.Models;
using System.Collections.Generic;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/khophong")]
    public class KhoPhongController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> Search([FromUri] Pagination pagination, [FromUri] string keywords = null)
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<KhoPhong> results = db.KhoPhong;
                if (pagination == null)
                    pagination = new Pagination();
                if (!pagination.includeEntities)
                { 
                }

                if (!string.IsNullOrWhiteSpace(keywords))
                    results = results.Where(x => x.TenKhoPhong.Contains(keywords));

                var res = results.Select(x => new { 
                    x.KhoPhongID,
                    x.PhanLoai,
                    x.TenKhoPhong,
                    x.DienTich,
                    x.DiaChi,
                    x.LaPhongChucNang,
                    x.NamDuaVaoSuDung,
                    TenGiaoVien = x.GiaoVien.Select(yx => yx.TenDayDu),
                    MonSuDung = x.KhoPhongBoMon.Select(y => y.MonHoc.TenMon)
                }).OrderBy(o => o.KhoPhongID);

                return Ok((await GetPaginatedResponse(res, pagination)));
            }
        } 

        [HttpGet, Route("{khoPhongID:int}")]
        public async Task<IHttpActionResult> Get(int khoPhongID)
        {
            using (var db = new ApplicationDbContext())
            {
                var khoPhong = await db.KhoPhong.Include(x => x.KhoPhongBoMon)
                    .SingleOrDefaultAsync(o => o.KhoPhongID == khoPhongID);

                if (khoPhong == null)
                    return NotFound();
                var res = new {
                    khoPhong.KhoPhongID, 
                    khoPhong.PhanLoai,
                    khoPhong.TenKhoPhong,
                    khoPhong.DienTich,
                    khoPhong.DiaChi,
                    khoPhong.LaPhongChucNang,
                    khoPhong.NamDuaVaoSuDung,
                    GiaoVienID = khoPhong.GiaoVien != null? (int?)khoPhong.GiaoVien.Select(yx => yx.GiaoVienID).FirstOrDefault() : null,
                    MonSuDung = khoPhong.KhoPhongBoMon != null ? khoPhong.KhoPhongBoMon.Select(y => y.MonHocID) : null
                };
                return Ok(res);
            }
        }

        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Insert([FromBody] KhoPhong khoPhong)
        {
            if (khoPhong.KhoPhongID != 0) return BadRequest("Invalid KhoPhongID");

            using (var db = new ApplicationDbContext())
            { 
                //khoPhong.KhoPhongBoMon.ToList().ForEach(x => x.KhoPhong = null);
                db.KhoPhong.Add(khoPhong);
                await db.SaveChangesAsync();
            }

            return Ok(khoPhong);
        }

        [HttpPut, Route("{khoPhongID:int}")]
        public async Task<IHttpActionResult> Update(int khoPhongID, [FromBody] KhoPhong khoPhong)
        {
            if (khoPhong.KhoPhongID != khoPhongID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new ApplicationDbContext())
            {
                var lstKhoPhongBoMonOld = db.KhoPhongBoMon.Where(x => x.KhoPhongID == khoPhongID);
                db.KhoPhongBoMon.RemoveRange(lstKhoPhongBoMonOld);

                var lstKhoPhongBoMonNew = new List<KhoPhongBoMon>();
                khoPhong.KhoPhongBoMon.ToList().ForEach(x =>
                {
                    lstKhoPhongBoMonNew.Add(new KhoPhongBoMon()
                    {
                        KhoPhongID = khoPhongID,
                        MonHocID = x.MonHocID
                    });
                });
                db.KhoPhongBoMon.AddRange(lstKhoPhongBoMonNew);
                khoPhong.KhoPhongBoMon = null;
                db.Entry(khoPhong).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.KhoPhong.Count(o => o.KhoPhongID == khoPhongID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(khoPhong);
            }
        }

        [HttpDelete, Route("{khoPhongID:int}")]
        public async Task<IHttpActionResult> Delete(int khoPhongID)
        {
            using (var db = new ApplicationDbContext())
            {
                var khoPhong = await db.KhoPhong.SingleOrDefaultAsync(o => o.KhoPhongID == khoPhongID);

                if (khoPhong == null)
                    return NotFound();

                db.Entry(khoPhong).State = EntityState.Deleted;
                await db.SaveChangesAsync();
                return Ok();
            }
        }

    }
}
