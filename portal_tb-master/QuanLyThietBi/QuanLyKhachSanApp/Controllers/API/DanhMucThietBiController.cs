using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Threading.Tasks;
using HVITCore.Controllers;
using System.Data.Entity.Infrastructure;
using CMS.Models;
using System.Collections.Generic;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/danhmucthietbi")]
    public class DanhMucThietBiController : BaseApiController
    {
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> Search([FromUri] Pagination pagination, [FromUri] string keywords = null)
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<DanhMucThietBi> results = db.DanhMucThietBi;
                if (pagination == null)
                    pagination = new Pagination();
                if (!pagination.includeEntities)
                {
                    results = results.Include(x => x.LoaiThietBi)
                                     .Include(x => x.MonSuDungTB).Include(x => x.KhoiSuDungTB.Select(y => y.KhoiHoc))
                                     .Include(x => x.KhoiSuDungTB).Include(x => x.MonSuDungTB.Select(y => y.MonHoc));
                }

                if (!string.IsNullOrWhiteSpace(keywords))
                    results = results.Where(x => x.TenThietBi.Contains(keywords) || x.MaThietBi.Contains(keywords));

                var res = results.Select(x => new {
                    x.DanhMucThietBiID,
                    x.LoaiThietBiID,
                    x.MaThietBi,
                    x.TenThietBi,
                    x.DonViTinh,
                    x.LaThietBiTieuHao,
                    x.LaThietBiTuLam,
                    x.LaQuanLyChiTiet,
                    TenLoaiThietBi = x.LoaiThietBi !=null? x.LoaiThietBi.TenLoaiThietBi : "",
                    KhoiSuDung = x.KhoiSuDungTB.Select(y => y.KhoiHoc.TenKhoi).ToList(),
                    MonSuDung = x.MonSuDungTB.Select(y => y.MonHoc.TenMon).ToList()
                }).OrderBy(o => o.DanhMucThietBiID);

                return Ok((await GetPaginatedResponse(res, pagination)));
            }
        }

        [HttpGet, Route("loaithietbi")]
        public async Task<IHttpActionResult> GetAllLoaiThietBi([FromUri] Pagination pagination, [FromUri] string keywords = null)
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<LoaiThietBi> results = db.LoaiThietBi;
                if (pagination == null)
                    pagination = new Pagination();
                if (!pagination.includeEntities)
                {
                     
                }

                if (!string.IsNullOrWhiteSpace(keywords))
                    results = results.Where(x => x.TenLoaiThietBi.Contains(keywords));

                results = results.OrderBy(o => o.LoaiThietBiID);

                return Ok((await GetPaginatedResponse(results, pagination)));
            }
        }

        [HttpGet, Route("{danhMucThietBiID:int}")]
        public async Task<IHttpActionResult> Get(int danhMucThietBiID)
        {
            using (var db = new ApplicationDbContext())
            {
                var danhMucThietBi = await db.DanhMucThietBi.Include(x => x.KhoiSuDungTB).Include(x => x.MonSuDungTB)
                    .SingleOrDefaultAsync(o => o.DanhMucThietBiID == danhMucThietBiID);

                if (danhMucThietBi == null)
                    return NotFound();

                var res = new
                {
                    danhMucThietBi.DanhMucThietBiID,
                    danhMucThietBi.LoaiThietBiID,
                    danhMucThietBi.MaThietBi,
                    danhMucThietBi.TenThietBi,
                    danhMucThietBi.DonViTinh,
                    danhMucThietBi.LaThietBiTieuHao,
                    danhMucThietBi.LaThietBiTuLam,
                    danhMucThietBi.LaQuanLyChiTiet,
                    KhoiSuDung = danhMucThietBi.KhoiSuDungTB.Select(y => y.KhoiHocID).ToList(),
                    MonSuDung = danhMucThietBi.MonSuDungTB.Select(y => y.MonHocID).ToList()
                };
                return Ok(res);
            }
        }

        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Insert([FromBody] DanhMucThietBi danhMucThietBi)
        {
            if (danhMucThietBi.DanhMucThietBiID != 0) return BadRequest("Invalid DanhMucThietBiID");
            danhMucThietBi.NgayTao = DateTime.Now;
            using (var db = new ApplicationDbContext())
            {
                List<KhoiSuDungTB> lstKhoiSuDung = new List<KhoiSuDungTB>();
                List<MonSuDungTB> lstMonSuDung = new List<MonSuDungTB>();

                if (!string.IsNullOrWhiteSpace(danhMucThietBi.KhoiApDung))
                {
                    foreach(var id in danhMucThietBi.KhoiApDung.Split(','))
                    {
                        lstKhoiSuDung.Add(new KhoiSuDungTB()
                        {
                            DanhMucThietBiID = danhMucThietBi.DanhMucThietBiID,
                            KhoiHocID = int.Parse(id),
                            DonViID = danhMucThietBi.DonViID,
                            NgayTao = DateTime.Now
                        });
                    }
                }
                if (!string.IsNullOrWhiteSpace(danhMucThietBi.MonApDung))
                {
                    foreach (var id in danhMucThietBi.MonApDung.Split(','))
                    {
                        lstMonSuDung.Add(new MonSuDungTB()
                        {
                            DanhMucThietBiID = danhMucThietBi.DanhMucThietBiID,
                            MonHocID = int.Parse(id),
                            DonViID = danhMucThietBi.DonViID,
                            NgayTao = DateTime.Now
                        });
                    }
                }
                danhMucThietBi.KhoiSuDungTB = lstKhoiSuDung;
                danhMucThietBi.MonSuDungTB = lstMonSuDung;
                db.DanhMucThietBi.Add(danhMucThietBi);
                await db.SaveChangesAsync();
            }

            return Ok(danhMucThietBi);
        }

        [HttpPut, Route("{danhMucThietBiID:int}")]
        public async Task<IHttpActionResult> Update(int danhMucThietBiID, [FromBody] DanhMucThietBi danhMucThietBi)
        {
            if (danhMucThietBi.DanhMucThietBiID != danhMucThietBiID) return BadRequest("Id mismatch");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var db = new ApplicationDbContext())
            { 
                var lstKhoiSuDungThietBiOld = db.KhoiSuDungTB.Where(x => x.DanhMucThietBiID == danhMucThietBiID);
                var lstMonSuDungThietBiOld = db.MonSuDungTB.Where(x => x.DanhMucThietBiID == danhMucThietBiID);

                db.KhoiSuDungTB.RemoveRange(lstKhoiSuDungThietBiOld);
                db.MonSuDungTB.RemoveRange(lstMonSuDungThietBiOld);

                List<KhoiSuDungTB> lstKhoiSuDung = new List<KhoiSuDungTB>();
                List<MonSuDungTB> lstMonSuDung = new List<MonSuDungTB>();

                if (!string.IsNullOrWhiteSpace(danhMucThietBi.KhoiApDung))
                {
                    foreach (var id in danhMucThietBi.KhoiApDung.Split(','))
                    {
                        lstKhoiSuDung.Add(new KhoiSuDungTB()
                        {
                            DanhMucThietBiID = danhMucThietBi.DanhMucThietBiID,
                            KhoiHocID = int.Parse(id),
                            DonViID = danhMucThietBi.DonViID,
                            NgayTao = DateTime.Now,
                            DanhMucThietBi = null
                        });
                    }
                }
                if (!string.IsNullOrWhiteSpace(danhMucThietBi.MonApDung))
                {
                    foreach (var id in danhMucThietBi.MonApDung.Split(','))
                    {
                        lstMonSuDung.Add(new MonSuDungTB()
                        {
                            DanhMucThietBiID = danhMucThietBi.DanhMucThietBiID,
                            MonHocID = int.Parse(id),
                            DonViID = danhMucThietBi.DonViID,
                            NgayTao = DateTime.Now,
                            DanhMucThietBi = null
                        });
                    }
                }

                db.KhoiSuDungTB.AddRange(lstKhoiSuDung);
                db.MonSuDungTB.AddRange(lstMonSuDung);
                db.Entry(danhMucThietBi).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ducEx)
                {
                    bool exists = db.DanhMucThietBi.Count(o => o.DanhMucThietBiID == danhMucThietBiID) > 0;
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ducEx;
                    }
                }

                return Ok(danhMucThietBi);
            }
        }

        [HttpDelete, Route("{danhMucThietBiID:int}")]
        public async Task<IHttpActionResult> Delete(int danhMucThietBiID)
        {
            using (var db = new ApplicationDbContext())
            {
                var danhMucThietBi = await db.DanhMucThietBi.SingleOrDefaultAsync(o => o.DanhMucThietBiID == danhMucThietBiID);

                if (danhMucThietBi == null)
                    return NotFound();

                db.Entry(danhMucThietBi).State = EntityState.Deleted;
                await db.SaveChangesAsync();
                return Ok();
            }
        }

    }
}
