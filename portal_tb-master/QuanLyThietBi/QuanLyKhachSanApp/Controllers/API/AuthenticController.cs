﻿using HVIT.Security;
using HVITCore.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Data.Entity;
using QuanLyKhachSanApp.Models;

namespace QuanLyKhachSanApp.Controllers
{
    [RoutePrefix("api/auth")]
    public class AuthController : BaseApiController
    {
        public class LoginForm
        {
            public string userName { get; set; }
            public string password { get; set; }
            public string RePassword { get; set; }
            public string ChangePasswordKey { get; set; }
            public string OldPassword { get; set; }
        }

        public class UserModel
        {
            public User User { get; set; }
            public List<int> Groups { get; set; }

            public UserModel()
            {
                Groups = new List<int>();
            }
        }

        [HttpPost]
        [Route("login")]
        public IHttpActionResult Login(LoginForm login)
        {
            using (TCEntities db = new TCEntities())
            {
                User user1 = db.Users.Where(x=>x.UserName == login.userName).Include(x => x.UserRoles.Select(o=>o.Role.RoleRoleDetails.Select(u=>u.RoleDetail))).FirstOrDefault();
                User user = db.Users.SingleOrDefault(x => x.UserName == login.userName);
                if (user != null)
                {
                    string passwordInput = AuthenticationHelper.getPassWord(login.password);
                    string passwordUser = user.PassWord;

                    if (passwordInput.Equals(passwordUser))
                    {
                       /* TokenProvider tokenProvider = new TokenProvider();
                        TokenIdentity token = tokenProvider.GenerateToken(user.UserID, login.Username,
                            Request.Headers.UserAgent.ToString(),
                            "", Guid.NewGuid().ToString(),
                            DateTime.Now.Ticks);
                        token.SetAuthenticationType("Custom");
                        token.SetIsAuthenticated(true);
                        db.AccessTokens.Add(new AccessTokens()
                        {
                            Token = token.Token,
                            EffectiveTime = new DateTime(token.EffectiveTime),
                            ExpiresIn = token.ExpiresTime,
                            IP = token.IP,
                            UserAgent = token.UserAgent,
                            UserName = token.Name
                        });
                        db.SaveChanges();*/
                        var users = db.Users
                            .Select(x => new
                            {
                                x.Id,
                                x.UserName,
                                x.FullName,
                                x.Mobile
                            })
                            .FirstOrDefault(x => x.Id == user.Id);
                        return Ok(
                            new
                            {
                                Profile = new
                                {
                                    UserId = user1.Id,
                                    Username = user1.UserName,
                                    FullName = user1.FullName,
                                    Roles = user1.UserRoles.Select(x => x.Role.Name),
                                    RoleDetails = user1,
                                    countUserRole = user1.UserRoles.Count(),
                                    countRoleRoleDetails = user1.UserRoles.Select(x=>x.Role.RoleRoleDetails.Count())
                                    //NhanVien = nhanVien,
                                }
                            }
                        );
                    }
                }
                return Ok("Login failed!");
            }
        }

   /*     [AuthorizeUser, HttpGet]
        [Route("logout")]
        public IHttpActionResult Logout(Guid UserID)
        {
            using (var db = new TCEntities())
            {
                var user = db.Users.Where(x => x.Id == UserID).FirstOrDefault();
                if (user == null)
                    return BadRequest("invalid UserID");
                db.SaveChanges();
                return Ok();
            }
        }*/

        [AuthorizeUser, HttpGet]
        [Route("validate-token")]
        public IHttpActionResult ValidateToken()
        {
            TokenIdentity tokenIdentity = ClaimsPrincipal.Current.Identity as TokenIdentity;
            return Ok();
        }

    }
    public class AllowCrossSiteJsonAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            filterContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            base.OnActionExecuting(filterContext);
        }
    }
}
