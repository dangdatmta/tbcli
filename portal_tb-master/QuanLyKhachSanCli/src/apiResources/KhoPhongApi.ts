import { HTTP } from '@/HTTPServices'
import { BaseApi } from './BaseApi'
import { PaginatedResponse,Pagination } from './PaginatedResponse'
import { KhoPhong } from '@/models/KhoPhong'
export interface KhoPhongApiSearchParams extends Pagination {
    keywords?:number; 
} 
class KhoPhongApi extends BaseApi {
    search(searchParams: KhoPhongApiSearchParams): Promise<PaginatedResponse<KhoPhong>> {

        return new Promise<PaginatedResponse<KhoPhong>>((resolve: any, reject: any) => {
            HTTP.get('khophong', {
                params: searchParams
            }).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    } 
    detail(id: number): Promise<KhoPhong> {
        return new Promise<KhoPhong>((resolve: any, reject: any) => {
            HTTP.get('khophong/' + id).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    update(id: number, khoPhong: KhoPhong): Promise<KhoPhong> {
        return new Promise<KhoPhong>((resolve: any, reject: any) => {
            HTTP.put('khophong/' + id, 
                khoPhong
            ).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    insert(khoPhong: KhoPhong): Promise<KhoPhong> {
        return new Promise<KhoPhong>((resolve: any, reject: any) => {
            HTTP.post('khophong', 
                khoPhong
            ).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    delete(id: number): Promise<KhoPhong> {
        return new Promise<KhoPhong>((resolve: any, reject: any) => {
            HTTP.delete('khophong/' + id)
            .then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
}
export default new KhoPhongApi();
