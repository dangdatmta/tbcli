import { HTTP } from '@/HTTPServices'
import { BaseApi } from './BaseApi'
import { PaginatedResponse,Pagination } from './PaginatedResponse'
import { KhoiHoc } from '@/models/KhoiHoc'
export interface KhoiHocApiSearchParams extends Pagination {
    keywords?:number; 
} 
class KhoiHocApi extends BaseApi {
    search(searchParams: KhoiHocApiSearchParams): Promise<PaginatedResponse<KhoiHoc>> {

        return new Promise<PaginatedResponse<KhoiHoc>>((resolve: any, reject: any) => {
            HTTP.get('khoihoc', {
                params: searchParams
            }).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    } 
    detail(id: number): Promise<KhoiHoc> {
        return new Promise<KhoiHoc>((resolve: any, reject: any) => {
            HTTP.get('khoihoc/' + id).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    update(id: number, khoiHoc: KhoiHoc): Promise<KhoiHoc> {
        return new Promise<KhoiHoc>((resolve: any, reject: any) => {
            HTTP.put('khoihoc/' + id, 
                khoiHoc
            ).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    insert(khoiHoc: KhoiHoc): Promise<KhoiHoc> {
        return new Promise<KhoiHoc>((resolve: any, reject: any) => {
            HTTP.post('khoihoc', 
                khoiHoc
            ).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    delete(id: number): Promise<KhoiHoc> {
        return new Promise<KhoiHoc>((resolve: any, reject: any) => {
            HTTP.delete('khoihoc/' + id)
            .then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
}
export default new KhoiHocApi();
