import { HTTP } from '@/HTTPServices'
import { BaseApi } from './BaseApi'
import { PaginatedResponse,Pagination } from './PaginatedResponse'
import { DanhMucThietBi } from '@/models/DanhMucThietBi'
import { LoaiThietBi } from '../models/LoaiThietBi';
export interface DanhMucThietBiApiSearchParams extends Pagination {
    keywords?:number;
    monHocID?: number;
    khoiHocID?: number;
    loaiThietBiID?: number;
}
export interface LoaiThietBiApiSearchParams extends Pagination {
    keywords?: number;
}
class DanhMucThietBiApi extends BaseApi {
    search(searchParams: DanhMucThietBiApiSearchParams): Promise<PaginatedResponse<DanhMucThietBi>> {

        return new Promise<PaginatedResponse<DanhMucThietBi>>((resolve: any, reject: any) => {
            HTTP.get('danhmucthietbi', {
                params: searchParams
            }).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    getLoaiThietBi(searchParams: LoaiThietBiApiSearchParams): Promise<PaginatedResponse<LoaiThietBi>> {

        return new Promise<PaginatedResponse<LoaiThietBi>>((resolve: any, reject: any) => {
            HTTP.get('danhmucthietbi/loaithietbi', {
                params: searchParams
            }).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    detail(id: number): Promise<DanhMucThietBi> {
        return new Promise<DanhMucThietBi>((resolve: any, reject: any) => {
            HTTP.get('danhmucthietbi/' + id).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    update(id: number, danhMucThietBi: DanhMucThietBi): Promise<DanhMucThietBi> {
        return new Promise<DanhMucThietBi>((resolve: any, reject: any) => {
            HTTP.put('danhmucthietbi/' + id, 
                danhMucThietBi
            ).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    insert(danhMucThietBi: DanhMucThietBi): Promise<DanhMucThietBi> {
        return new Promise<DanhMucThietBi>((resolve: any, reject: any) => {
            HTTP.post('danhmucthietbi', 
                danhMucThietBi
            ).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    delete(id: number): Promise<DanhMucThietBi> {
        return new Promise<DanhMucThietBi>((resolve: any, reject: any) => {
            HTTP.delete('danhmucthietbi/' + id)
            .then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
}
export default new DanhMucThietBiApi();
