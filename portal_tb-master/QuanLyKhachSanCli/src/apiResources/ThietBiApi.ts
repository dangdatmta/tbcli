import { HTTP } from '@/HTTPServices'
import { BaseApi } from './BaseApi'
import { PaginatedResponse,Pagination } from './PaginatedResponse'
import { ThietBi } from '@/models/ThietBi'
export interface ThietBiApiSearchParams extends Pagination {
    keywords?:number; 
} 
class ThietBiApi extends BaseApi {
    search(searchParams: ThietBiApiSearchParams): Promise<PaginatedResponse<ThietBi>> {

        return new Promise<PaginatedResponse<ThietBi>>((resolve: any, reject: any) => {
            HTTP.get('thietbi', {
                params: searchParams
            }).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    } 
    detail(id: number): Promise<ThietBi> {
        return new Promise<ThietBi>((resolve: any, reject: any) => {
            HTTP.get('thietbi/' + id).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    update(id: number, thietBi: ThietBi): Promise<ThietBi> {
        return new Promise<ThietBi>((resolve: any, reject: any) => {
            HTTP.put('thietbi/' + id, 
                thietBi
            ).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    insert(thietBi: ThietBi): Promise<ThietBi> {
        return new Promise<ThietBi>((resolve: any, reject: any) => {
            HTTP.post('thietbi', 
                thietBi
            ).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    delete(id: number): Promise<ThietBi> {
        return new Promise<ThietBi>((resolve: any, reject: any) => {
            HTTP.delete('thietbi/' + id)
            .then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            })
        });
    }
}
export default new ThietBiApi();
