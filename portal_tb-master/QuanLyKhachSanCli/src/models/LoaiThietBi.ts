import { DanhMucThietBi } from "./DanhMucThietBi";

export interface LoaiThietBi {
    LoaiThietBiID: number;
    TenLoaiThietBi: string; 
    DanhMucThietBi?: DanhMucThietBi[];
}
