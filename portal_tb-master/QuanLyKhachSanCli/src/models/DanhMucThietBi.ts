import { LoaiThietBi } from "./LoaiThietBi";

export interface DanhMucThietBi {
    DanhMucThietBiID: number;
    LoaiThietBiID: number;
    TenThietBi: string;
    MaThietBi: string;
    MaThongKe: string;
    DonViTinh: string;
    MonApDung: string;
    KhoiApDung: string;
    MoTa: string;
    LaThietBiTieuHao?: boolean;
    LaThietBiTuLam?: boolean;
    LaQuanLyChiTiet?: boolean;
    SoLuongToiThieu: number;
    LoaiThietBi: LoaiThietBi; 
}
