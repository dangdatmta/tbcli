import { KhoPhong } from "./KhoPhong";
import { MonHoc } from "./MonHoc";

 

export interface KhoPhongBoMon {
    KhoPhongBoMonID: number; 
    KhoPhongID?: number; 
    MonHocID?: number;
    KhoPhong?: KhoPhong;
    MonHoc?: MonHoc;
}
