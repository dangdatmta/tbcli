 
export interface PhieuGhiGiam {
    PhieuGhiGiamID: number;
    MaPhieu: string;
    ChungTuLienQuan: string;
    NgayLapPhieu: Date;
    GhiChu: string;
    NguoiLapPhieuID: number;
    ChiTietPhieu: string;
    NamHoc: number;
}
