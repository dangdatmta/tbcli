import { BanKiemKe } from "./BanKiemKe";

 
export interface PhieuKiemKe {
    PhieuKiemKeID: number;
    MaPhieu: string;
    ChungTuLienQuan: string;
    NgayLapPhieu?: Date;
    NgayKiemKe?: Date;
    GhiChu: string;
    NguoiLapPhieuID: number;
    ChiTietPhieu: string;
    NamHoc: number;
    BanKiemKe?: BanKiemKe[];
}
