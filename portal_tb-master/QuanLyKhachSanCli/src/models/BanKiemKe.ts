import { PhieuKiemKe } from "./PhieuKiemKe";

export interface BanKiemKe {
    BanKiemKeID: number;
    NhanSuID?: number; 
    PhieuKiemKeID?: number;
    VaiTro: string;
    PhieuKiemKe?: PhieuKiemKe;
}
