 
export interface PhanCongGiangDay {
    PhanCongGiangDayID: number;
    GiaoVienID: number;
    MonHocID: number;
    LopHocID: number;
    KyHoc?: number;
    NamHoc?: number;
}
