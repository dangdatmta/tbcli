 
export interface PhieuNhapKho {
    PhieuNhapID: number;
    NgayNhap: Date;
    TongTien: number;
    NhanVienID: number; 
}
