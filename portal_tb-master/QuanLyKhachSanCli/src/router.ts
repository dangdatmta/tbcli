import Vue from 'vue';
import Router, { Route } from 'vue-router';
import DanhSachDanhMucThietBi from './components/DanhMucThietBi/DanhSachDanhMucThietBi.vue'; 
import DanhSachKhoPhong from './components/KhoPhong/DanhSachKhoPhong.vue'; 
import DanhSachLopHoc from './components/LopHoc/DanhSachLopHoc.vue'; 
import DanhSachKhoiHoc from './components/KhoiHoc/DanhSachKhoiHoc.vue'; 
import DanhSachMonHoc from './components/MonHoc/DanhSachMonHoc.vue'; 
import DanhSachGhiTang from './components/GhiTang/DanhSachGhiTang.vue'; 
import DanhSachThietBi from './components/ThietBi/DanhSachThietBi.vue'; 

import Login from './components/Login/Login.vue';
import store from './store/store';
import { HTTP } from './HTTPServices';
import { debug } from 'util';



Vue.use(Router);
export default new Router({
    routes: [
        {
            path: '/login',
            name: 'login',
            component: Login,
        }, 
        {
            path: '/danhmucthietbi',
            name: 'danhmucthietbi',
            component: DanhSachDanhMucThietBi,
            beforeEnter: guardRoute
        }, 
        {
            path: '/khophong',
            name: 'khophong',
            component: DanhSachKhoPhong,
            beforeEnter: guardRoute
        }, 
        {
            path: '/lophoc',
            name: 'lophoc',
            component: DanhSachLopHoc,
            beforeEnter: guardRoute
        }, 
        {
            path: '/khoihoc',
            name: 'khoihoc',
            component: DanhSachKhoiHoc,
            beforeEnter: guardRoute
        }, 
        {
            path: '/monhoc',
            name: 'monhoc',
            component: DanhSachMonHoc,
            beforeEnter: guardRoute
        }, 
        {
            path: '/thietbi',
            name: 'thietbi',
            component: DanhSachThietBi,
            beforeEnter: guardRoute
        }, 
        {
            path: '/ghitang',
            name: 'ghitang',
            component: DanhSachGhiTang,
            beforeEnter: guardRoute
        }, 
    ],
});


function guardRoute(to: Route, from: Route, next: any): void { 
    const isAuthenticated = store.state.user && store.state.user.AccessToken ? store.state.user.AccessToken.IsAuthenticated : false;
    if (!isAuthenticated) {
        next({
            path: '/login',
            query: {
                redirect: to.fullPath
            }
        });
    } else {
        HTTP.get('/auth/validate-token/')
            .then(response => {
                next();
            })
            .catch(e => {
                store.commit('CLEAR_ALL_DATA');
                next({
                    path: '/login',
                    query: {
                        redirect: to.fullPath
                    }
                });
            });
    }
}