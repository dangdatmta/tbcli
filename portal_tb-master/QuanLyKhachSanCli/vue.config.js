module.exports = {
    baseUrl: process.env.NODE_ENV === 'production' ? '/' : '/',
    outputDir: "../QuanLyKhachSanApp/",
    indexPath: "Views/Home/Index.cshtml"
}